from os import path
from Data_Retrieval import *
from datetime import date, datetime, time, timedelta,date
import multiprocessing
import threading
from Etch_prediction import *
from Data_Retrieval_Sapphire import *
import logging
from logging.handlers import TimedRotatingFileHandler
from Snowflake_IO import *

prediction_output_file = "Prediction_result.csv"
prediction_contributing_factors_file='Prediction_contribution.csv'
ptms_file="SAMPLE_DATA/ETX93x Part History 01-01-20 to 10-01-20.xlsx"
fdc_dir="SAMPLE_DATA/FDCKN_Data/"
wafer_dir="SAMPLE_DATA/Wafer_History/"
inline_dir="SAMPLE_DATA/Inline_Space_data/"
etch_dir="SAMPLE_DATA/Post_Etch_All/"
model_dir="models/"
model = "MLP"
live_prediction = True #This changes how PTMS data is read between a live prediction and historical prediction

logging.basicConfig(
        handlers=[TimedRotatingFileHandler('./ETX_93x_Prediction.log', when='D',interval=1, backupCount=30)],
        level=logging.INFO,
        format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s] %(message)s",
        datefmt='%Y-%m-%dT%H:%M:%S')

log = logging.getLogger(__name__)

def generate_dates(start_date,end_date):
    log.info('Generating dates')
    return pd.date_range(start_date,end_date, freq='D').strftime("%d-%m-%Y").tolist()

def start():

    start_time = datetime.now()

    #Auto generate dates here 
    
    dates = generate_dates('09-30-2020','09-30-2020')#mm-dd-yyyy    
    #tool_chamber_list=['ETX29305-PM1','ETX29305-PM2','ETX29305-PM3','ETX29305-PM4','ETX29305-PM5']   

    #dates=['10-07-2019']     
    tool_chamber_list=['ETX29300-PM2']

    if os.path.exists(prediction_output_file):
        os.remove(prediction_output_file)
    
    if os.path.exists(prediction_contributing_factors_file):
        os.remove(prediction_contributing_factors_file)
    
    log.info("cleaning up previous result files")

    date_eqp_list = []
    for date in dates:
        for item in tool_chamber_list:
            date_eqp_list.append((date,item))    

   
    # for item in date_eqp_list:
    #     execute(item)
    
    pool = multiprocessing.Pool(processes=1) 
    pool.map(execute,date_eqp_list)

    end_time = datetime.now()
    log.info("All done, total time taken is %s", end_time-start_time)   
    

def execute(date_tool_chamber):    

    date = date_tool_chamber[0]
    tool_chamber = date_tool_chamber[1]
    
    tool = tool_chamber.split('-')[0]
    chamber = tool_chamber.split('-')[1]
    log.info("Prediction for %s,%s", date,tool_chamber)           
    

    try:
        run_prediction(tool_chamber,date,ptms_file,fdc_dir,wafer_dir,inline_dir,etch_dir,model_dir,model,prediction_output_file,prediction_contributing_factors_file,live_prediction) # Format dd-mm-yyyy  # 7-3-1 Model's END DATE
    except Exception as e:
        log.exception(e)
        print("Exception in starter execute",e)

    return
    
    # if path.exists(outputfile):
    #     result.to_csv(outputfile, mode='a', header=False,index=False)
    #     print("Prediction Results written to file")
        
    # else:
    #     result.to_csv(outputfile,index=False)
    #     print("Prediction Results written to file")
    
    
    # print(result)

# tool_chamber = "ETX29301-PM4"
# to_date='10-07-2019'
# to_date = datetime.strptime(to_date, '%d-%m-%Y')
# from_date = to_date - pd.Timedelta(days=7)
# ptms_df = getPTMSData(from_date,to_date,tool_chamber)

# module_id = ptms_df['Module ID'].iloc[0]

# getFDCKNData(from_date,to_date,module_id,tool_chamber)

def test():
    log.info("Starting test")
    start=date(2020, 4, 8)
    end=date(2020, 4, 15)
    tool_chamber = 'ETX29300-PM2'
    #populateLotIds(tool_chamber.split('-')[0],start,end)
    #getFDCKNDataFromSapphire(tool_chamber.split('-')[0],[],tool_chamber,start,end)
    #getInlineSpaceDataFromGEMD(tool_chamber,[],start,end)
    #getWaferHistoryDataFromSapphire(tool_chamber,[],start,end)
    #getPostEtchCFMDataFromCFM(tool_chamber,[],start,end)

    #getRFOnHoursFromXsiteInGemD(tool_chamber, start,end)
    #readPTMSExportFile("SAMPLE_DATA/ReportGeneration.xls")
    #snowflakeReadTest()
    writeDFtoTable(pd.read_csv('Prediction_contribution.csv'),'PREDICTION_CONTRIBUTION')


if __name__=='__main__':    
    log.info('In main')    
    start()
    #test()