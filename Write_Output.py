import pandas as pd
import os
from starter import prediction_output_file, prediction_contributing_factors_file
import logging

log = logging.getLogger(__name__)

def write_results_to_file(df,filename):
    log.info("Writing results to %s",filename)

    df.columns= df.columns.str.upper()
    
    if os.path.exists(filename): 
        df.to_csv(filename, mode='a',index =False,header=True)
    else:
       df.to_csv(filename,index =False,header=True)
    
