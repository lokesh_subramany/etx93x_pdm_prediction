import pandas as pd
import xlrd
import os
from datetime import date, datetime, time, timedelta
from pyathena import connect
from pyathena.pandas_cursor import PandasCursor
import sys
from Data_Retrieval_Sapphire import *
import logging

log = logging.getLogger(__name__)

conn = connect(s3_staging_dir='s3://gf-f8-dev-ais-team-bucket/queryresults/',region_name='us-east-1')

cursor = connect(s3_staging_dir='s3://gf-f8-dev-ais-team-bucket/queryresults/',
                 region_name="us-east-1",
                 cursor_class=PandasCursor).cursor()

def compute_months(first_date, second_date):
    year1, month1, year2, month2 = map(
        int, 
        (first_date[:4], first_date[5:7], second_date[:4], second_date[5:7])
    )

    return [
        '{:0>4}-{:0>2}'.format(year, month)
        for year in range(year1, year2 + 1)
        for month in range(month1 if year == year1 else 1, month2 + 1 if year == year2 else 13)
    ]

month_dict = {'01':'jan', '02':'feb','03':'mar', '04':'apr','05':'may', '06':'jun','07':'jul', '08':'aug','09':'sep', '10':'oct','11':'nov', '12':'dec'}

def getPTMSDataFromFile(ptms_file,from_date,to_date,tool_chamber,live_prediction):
    log.info("Reading PTMS file %s", ptms_file)

    ptms_file_name = ptms_file
    # For Reading All sheets from Customer's PTMS Excel Data
    req_lampn = ['719-003481-858', '715-045710-821', '715-042721-866', '715-027638-822', '714-045743-809', '000-300101-956']
    ptms_fields = ['Tool ID', 'Module ID', 'Lam P/N', 'Serial Number', 'Install Date', 'Removal Date', 'Usage At Install', 'Usage At Removal', 'Usage During This Install']   

    xls = xlrd.open_workbook(ptms_file_name, on_demand=True) # READ EXCEL FILE TO GET SHEET NAMES
    sheet_names = xls.sheet_names() # Now you can list all sheets in the file
    ptms_df = pd.DataFrame()
    for sheet in sheet_names:
        temp_df = pd.read_excel(ptms_file_name, sheet_name=sheet, skiprows=3, usecols=ptms_fields)
        ptms_df = ptms_df.append(temp_df)

    ptms_df.drop_duplicates(keep='first', inplace=True)
    
    ptms_df['Install Date'] = pd.to_datetime(ptms_df['Install Date'], format='%m/%d/%Y %I:%M:%S %p')
    ptms_df['Removal Date'] = pd.to_datetime(ptms_df['Removal Date'], format='%m/%d/%Y %I:%M:%S %p')
    ptms_df= ptms_df[ptms_df['Lam P/N'].isin(req_lampn)]

    if live_prediction:
        log.info('Live prediction selected')
        ptms_df = ptms_df[(ptms_df['Removal Date'].isna()) & (ptms_df['Usage At Removal'].isna()) & (ptms_df['Module ID'] == tool_chamber)] ## FOR LIVE DATA PREDICTION
    else:
        log.info('Historical prediction selected')
        ptms_df = ptms_df[(ptms_df['Install Date'] <=  from_date) & (ptms_df['Removal Date'] >=  to_date) & (ptms_df['Module ID'] == tool_chamber)] ## FOR HISTORICAL DATA PREDICTION

    ptms_df = ptms_df[ptms_fields]
    ptms_df.reset_index(drop=True, inplace=True)

    if ptms_df.shape[0] > 0:
        print("Read PTMS File, No of Parts For Prediction: ", ptms_df.shape[0])
        log.info("Read PTMS File, No of Parts For Prediction: %d", ptms_df.shape[0])
    else:
        print("PTMS Data doesn't contain required LAM-PN's or Machine;s/Tool's")
        log.error("PTMS Data doesn't contain required LAM-PN's or Machine;s/Tool's")
        raise Exception("PTMS Data doesn't contain required LAM-PN's or Machine;s/Tool's")
        #sys.exit()

    ## If Prior_Usage is Taken from the User then change 'prior_usage_UserInput' to 'True
    prior_usage_UserInput = False
    if prior_usage_UserInput:
        ptms_df = ptms_df[['Tool ID', 'Module ID', 'Lam P/N', 'Serial Number', 'Usage At Install', 'Install Date']]
        save_ptms_df = ptms_df[['Serial Number']].copy()
        save_ptms_df.insert(len(save_ptms_df.columns), "Prior_Usage", "")
        save_ptms_df.to_csv("Prior_Usage.csv", index = False)
        print("\nKindly fill 'Prior_Usage' column in 'Prior_Usage.csv' file, which is in present working directory and save the file")
        continue_ = input("\tIf the CSV file is updated press 1 to continue or 0 to exit:\t")
        if not int(continue_):
            print("Terminating Program")            
            raise Exception("Terminating program")
            #sys.exit()

        save_ptms_df = pd.read_csv("Prior_Usage.csv")
        ptms_df = pd.merge(ptms_df, save_ptms_df, on='Serial Number', how='left')
        if ptms_df.shape[0] > 0:
            ptms_df['Install Date'] = pd.to_datetime(ptms_df['Install Date'])
            print("\tUpdated 'Prior_Usage.csv' File")
            print("No of Parts For Prediction: ", ptms_df.shape[0])
        else:
            print("PTMS Data doesn't contain required LAM-PN's or Machine's/Tool's")
            raise Exception("PTMS Data doesn't contain required LAM-PN's or Machine's/Tool's")
            #sys.exit()
    ## Automating Prior_Usage calculation ONLY FOR HISTORICAL DAYA PREDICTION
    else:
        ptms_df['Prior_Usage'] = 0

        new_prior_usage = getRFOnHoursFromXsiteInGemD(tool_chamber,from_date,to_date)
        for ind, row in ptms_df.iterrows():
            # install_date = row['Install Date']
            # removal_date = row['Removal Date']
            # total_days = (removal_date-install_date).days
            # usage_at_install = row['Usage At Install']
            # usage_at_removal = row['Usage At Removal']
            # usage = row['Usage During This Install']
            # no_of_days_prior_observation = (from_date - install_date).days
            # prior_usage = round(usage*no_of_days_prior_observation/total_days, 1)
            
            ptms_df['Prior_Usage'].iloc[ind] = new_prior_usage
    
    #print(ptms_df.head())

    return ptms_df

def getFDCKNDataFromFile(from_date,to_date,module_id,tool_chamber):
    print("In getFDCKNDataFromFile")
    fdc_files_path = "SAMPLE_DATA/FDCKN_Data/"
    part_start_time = datetime.now()
    check_tool = []

    if '-' in tool_chamber:
        tool = tool_chamber.split('-')[0]
        chamber = tool_chamber.split('-')[1]
        check_tool = [tool]
    else:
        print("Tool-Chamber(ETX2930X-PMX) both are required")
        sys.exit()

    if fdc_files_path[-1] != "/":
        fdc_files_path = fdc_files_path + "/"

    available_fdc_files = os.listdir(fdc_files_path)
    fdc_files = [file_ for file_ in available_fdc_files if 'ETX' in file_]
    
    tool_id = check_tool[0]

    start = from_date
    end = to_date
    data_start_time = from_date
    data_end_time = to_date
    date_list = compute_months(str(data_start_time), str(data_end_time))
    # Read Required FDC File/Files
    req_files_ = []
    master_fdc_df = pd.DataFrame()
    
    fdc_df = pd.DataFrame()
    read_fdc_files_list = []
        
    for filename_ in fdc_files:
        if tool_id in filename_:
            for date_ in date_list:
                year = str(date_).split('-')[0]
                month = str(date_).split('-')[1]
                month = month_dict[month]
                check_date = str(year) in filename_ and month in filename_.lower()
                if check_date :
                    read_fdc_files_list.append(filename_)
                    temp_fdc = pd.read_csv(fdc_files_path + filename_, low_memory=False)
                    read_fdc_files = True
                    temp_fdc = temp_fdc.loc[temp_fdc['TOOL_CHAMBER'] == module_id]
                    temp_fdc.reset_index(drop=True, inplace=True)
                    if temp_fdc.shape[0]>0:
                        for date_col in temp_fdc.columns:
                            if "TIME" in date_col:
                                remove_index_list = []
                                for ind, element in enumerate(temp_fdc[date_col]):
                                    if 'TIME' in element:
                                        remove_index_list.append(ind)
                                    else:
                                        date = element.split(' ')[0]
                                        year = date.split('-')[0]
                                        if int(year) < 2019:
                                            remove_index_list.append(ind)
                                if len(remove_index_list) > 0:
                                    temp_fdc.drop(temp_fdc.index[remove_index_list], inplace=True)
                        temp_fdc.reset_index(drop=True, inplace=True)
                    temp_fdc['START_TIME'] =  pd.to_datetime(temp_fdc['START_TIME'])
                    temp_fdc['END_TIME'] =  pd.to_datetime(temp_fdc['END_TIME'])
                    temp_fdc = temp_fdc.loc[(temp_fdc['START_TIME']>= data_start_time) & (temp_fdc['END_TIME']<= data_end_time)]
                    temp_fdc.reset_index(drop=True, inplace=True)
                    fdc_df = fdc_df.append(temp_fdc)
    
    part_end_time = datetime.now()
    time_per_part = part_end_time- part_start_time

    print("FDC_KN Files Read: ", read_fdc_files_list, "Time taken to read FDC_KN file : ", time_per_part)
    print("No of Records in FDC_KN Data between From_date & To_Date: ",fdc_df.shape[0])

    if fdc_df.shape[0] == 0:
        print("FDC Data is empty")
        print("Terminating Program")
        sys.exit()

    #print(fdc_df.head())
    return fdc_df

def getFDCKNDataFromAthena(tool_id, date_list, module_id, data_start_time, data_end_time):
    print('Getting FDCKN data from athena')

    SQL="""
    select MFG_AREA_NAME,TECHNOLOGY,PRODUCT_GROUP_ID,PRODUCT_ID,LOT_ID,SCRIBE,WAFER_NUMBER,CTRL_JOB,STEP,RECIPE_MACHINE_ID,RECIPE_LOGICAL_ID,FDC_CONTEXT_ID,RECIPE_NAME,APPLICATION_NAME,APPLICATION_VERSION,MODEL_STATUS,STEP_ID,START_TIME,END_TIME,FDC_RAW,SENSOR_NAME,TOOL_CHAMBER,EQPT_HARDWARE_CLASS,EQPT_HARDWARE_TYPE from fab8_etx29x_prediction.fdckn 
    where tool_chamber ='"""
    SQL+=module_id
    SQL+="""' and start_time between TIMESTAMP '"""
    SQL+= data_start_time.strftime("%Y-%m-%d %H:%M:%S")
    SQL+="""' and TIMESTAMP '"""
    SQL+= data_end_time.strftime("%Y-%m-%d %H:%M:%S")
    SQL+="""'"""
    
    query_start = datetime.now()
    
    #print(SQL)
    
    #FDC_df = pd.read_sql(SQL,conn)
    FDC_df = cursor.execute(SQL).as_pandas()
    # FDC_df['START_TIME'] =  pd.to_datetime(FDC_df['START_TIME'])
    # FDC_df['END_TIME'] =  pd.to_datetime(FDC_df['END_TIME'])

    print("No of Records in FDC_KN Data between From_date & To_Date: ",FDC_df.shape[0])

    query_end = datetime.now()
    print("FDC query time is ",query_end - query_start)

    if FDC_df.shape[0] == 0:
        print("FDC Data is empty")
        print("Terminating Program")
        raise Exception("FDC data is empty")
        #sys.exit()
    
    return FDC_df
    

def getInlineSpaceDataFromFile(tool_chamber):
    print("Getting inline space data from file")

    inline_space_path = "SAMPLE_DATA/Inline_Space_data/"    

    if '-' in tool_chamber:
        req_tool = tool_chamber.split('-')[0]
    else:
        req_tool = tool_chamber

    if inline_space_path[-1] != '/':
        inline_space_path = inline_space_path + '/'

    re_IS_cols = ['TOOL', 'CHAMBER', 'SCRIBE', 'PROCESS_STEP', 'RAW_VALUE','LSL', 'USL']
    inline_space_files = os.listdir(inline_space_path)
    inline_space_df = pd.DataFrame()

    for file_ in inline_space_files:
        temp_ = pd.read_csv(inline_space_path + file_, usecols = re_IS_cols)
        temp_ = temp_[temp_['TOOL'] == req_tool]
        inline_space_df = inline_space_df.append(temp_)
    
    print(inline_space_df.head(10))
    return inline_space_df

def getInlineSpaceDataFromAthena(tool_chamber,req_scribe_list,from_date,to_date):
    print('Getting Inline space data from athena')
    if '-' in tool_chamber:
        req_tool = tool_chamber.split('-')[0]
    else:
        req_tool = tool_chamber

    scribe_string = ""
    for scribe in req_scribe_list:
        scribe_string+='\''+scribe+'\''+','
    scribe_string = scribe_string[:-1]

    SQL="""select TOOL,CHAMBER,SCRIBE,PROCESS_STEP,RAW_VALUE,LSL,USL from fab8_etx29x_prediction.inline_space where TOOL = '"""
    SQL+= tool_chamber.split('-')[0]
    SQL+="""' AND CHAMBER = '"""
    SQL+=tool_chamber.split('-')[1]
    SQL+="""' and SCRIBE IN ("""
    SQL+= scribe_string
    SQL+=""")"""
    #print(SQL)

    #inline_space_df = pd.read_sql(SQL,conn)
    inline_space_df = cursor.execute(SQL).as_pandas()
    

    #print(inline_space_df.head(10))
    return inline_space_df

def getWaferHistoryDataFromFile(tool_chamber,req_scribe_list):
    print("Getting wafer history data from file")

    wafer_path = "SAMPLE_DATA/Wafer_History/"

    if '-' in tool_chamber:
        req_tool = tool_chamber.split('-')[0]
    else:
        req_tool = tool_chamber  

    if wafer_path[-1] != '/':
        wafer_path = wafer_path + '/'

    wafer_files = os.listdir(wafer_path)
    wafer_cols = ['SCRIBE', 'STEP', 'TOOL', 'DATE_STEP_STARTED', 'DATE_STEP_FINISHED']
    wafer_df = pd.DataFrame()

    for file_ in wafer_files:
        if '.csv' in file_:
            temp_ = pd.read_csv(wafer_path + file_)
            temp_ = temp_[temp_['TOOL'] == req_tool]
            temp_ = temp_[wafer_cols]
            temp_ = temp_[temp_['SCRIBE'].isin(req_scribe_list)]
            temp_.drop_duplicates(keep='first', inplace=True)
            wafer_df = wafer_df.append(temp_)
        else:
            with open(wafer_path + file_,encoding='UTF-16') as f:
                temp_ = pd.read_csv(f)
                temp_ = temp_[temp_['TOOL'] == req_tool]
                temp_ = temp_[wafer_cols]
                temp_ = temp_[temp_['SCRIBE'].isin(req_scribe_list)]
                temp_.drop_duplicates(keep='first', inplace=True)
                wafer_df = wafer_df.append(temp_)
    
    return wafer_df

def getWaferHistoryDataFromAthena(tool_chamber,req_scribe_list,from_date,to_date):

    print('Getting wafer history from athena')
    #print(tool_chamber,from_date,to_date)

    SQL="""
    select SCRIBE,STEP,TOOL,DATE_STEP_STARTED,DATE_STEP_FINISHED from fab8_etx29x_prediction.wafer_history where tool = '"""
    SQL+= tool_chamber.split('-')[0]
    SQL+="""' and date_step_started BETWEEN TIMESTAMP '"""
    SQL+= from_date.strftime("%Y-%m-%d %H:%M:%S")
    SQL+="""' and TIMESTAMP '"""
    SQL+= to_date.strftime("%Y-%m-%d %H:%M:%S")
    SQL+="""'"""

    #print(SQL)

    #wafer_df = pd.read_sql(SQL,conn)
    wafer_df = cursor.execute(SQL).as_pandas()
    wafer_df = wafer_df[wafer_df['SCRIBE'].isin(req_scribe_list)]
    wafer_df.drop_duplicates(keep='first',inplace=True)

    #print(wafer_df.head(10))

    return wafer_df

def getPostEtchCFMDataFromFile(tool_chamber,req_scribe_list):
    print('Getting CFM data from file')

    if '-' in tool_chamber:
        req_tool = tool_chamber.split('-')[0]
    else:
        req_tool = tool_chamber  

    post_etch_old_path = "SAMPLE_DATA/Post_Etch_Data/Post_Etch_Non14LPP/"
    post_etch_new_path = "SAMPLE_DATA/Post_Etch_Data/Post_Etch14LPP/"

    same_post_etch_path = False
    
    if post_etch_old_path == post_etch_new_path:
        same_post_etch_path = True
    req_post_etch_cols = ['SCRIBE', 'LAYER', 'INSPECTIONTIME', 'TDC']
    if post_etch_old_path[-1] != '/':
        post_etch_old_path = post_etch_old_path + '/'
    post_etch_files = os.listdir(post_etch_old_path)
    post_etch_cols = ['SCRIBE', 'LAYER', 'TDC', 'INSPECTIONTIME']
    req_layers = ['I1-CFMRIEHMTINBF-14LPP', 'I2-CFMRIEHMTINBF-14LPP', 'I2-CFMRIEHMTINDF-14LPP', 'I3-CFMRIEHMTINBF-14LPP',
                'I3-CFMRIEHMTINDF-14LPP', 'I2-CFMRIEHMTINDF-14HP']
    replace_layer = ['I1-RIEHMTIN-14LPP.01', 'I2-RIEHMTIN-14LPP.01', 'I2-RIEHMTIN-14LPP.01', 'I3-RIEHMTIN-14LPP.01',
                    'I3-RIEHMTIN-14LPP.01', 'I2-RIEHMTIN-14HP.01']
    post_etch_df = pd.DataFrame()
    for file_ in post_etch_files:
        if req_tool.lower() in file_.lower():
            temp_ = pd.read_csv(post_etch_old_path + file_, usecols = req_post_etch_cols)
            temp_ = temp_[temp_['LAYER'].isin(req_layers)]
            temp_['TOOL'] = file_.split('_')[2]
            for i, layer in enumerate(req_layers):
                each_layer_df = temp_[temp_['LAYER']==layer]
                each_layer_df['LAYER'] = replace_layer[i]
                post_etch_df = post_etch_df.append(each_layer_df)
                post_etch_df.reset_index(drop=True, inplace=True)  
    
    post_etch_df = post_etch_df[post_etch_df['LAYER'].isin(req_scribe_list)]
    post_etch_df.reset_index(drop=True, inplace=True)
    print("READ POST_ETCH 14LPP DATA")

    if not same_post_etch_path:
        if post_etch_new_path[-1] != '/':
            post_etch_new_path = post_etch_new_path + '/'
        post_etch_files = os.listdir(post_etch_new_path)
        for file_ in post_etch_files:
            if req_tool.lower() in file_.lower():
                temp_ = pd.read_csv(post_etch_new_path + file_, usecols = req_post_etch_cols)
                temp_ = temp_[temp_['LAYER'].isin(req_layers)]
                temp_['TOOL'] = file_[27:35]
                for i, layer in enumerate(req_layers):
                    each_layer_df = temp_[temp_['LAYER']==layer]
                    each_layer_df['LAYER'] = replace_layer[i]
                    post_etch_df = post_etch_df.append(each_layer_df)
                    post_etch_df.reset_index(drop=True, inplace=True)
        post_etch_df = post_etch_df[post_etch_df['LAYER'].isin(req_scribe_list)]
        post_etch_df.reset_index(drop=True, inplace=True)
        print("READ POST_ETCH NON-14LPP DATA")
    
    return post_etch_df

def getPostEtchCFMDataFromAthena(tool_chamber, req_scribe_list, from_date, to_date):
    print('Getting CFM data from athena')

    SQL="""
    select SCRIBE, LAYER, INSPECTIONTIME, NDD, DEFECTDESCRIPTION from fab8_etx29x_prediction.cfm where layer in ('I1-CFMRIEHMTINBF-14LPP', 'I2-CFMRIEHMTINBF-14LPP', 'I2-CFMRIEHMTINDF-14LPP', 'I3-CFMRIEHMTINBF-14LPP','I3-CFMRIEHMTINDF-14LPP', 'I2-CFMRIEHMTINDF-14HP') 
    and INSPECTIONTIME BETWEEN TIMESTAMP '"""
    SQL+= from_date.strftime("%Y-%m-%d %H:%M:%S")
    SQL+="""' and TIMESTAMP '"""
    SQL+= to_date.strftime("%Y-%m-%d %H:%M:%S")
    SQL+="""'"""

    req_layers = ['I1-CFMRIEHMTINBF-14LPP', 'I2-CFMRIEHMTINBF-14LPP', 'I2-CFMRIEHMTINDF-14LPP', 'I3-CFMRIEHMTINBF-14LPP',
                'I3-CFMRIEHMTINDF-14LPP', 'I2-CFMRIEHMTINDF-14HP']
    replace_layer = ['I1-RIEHMTIN-14LPP.01', 'I2-RIEHMTIN-14LPP.01', 'I2-RIEHMTIN-14LPP.01', 'I3-RIEHMTIN-14LPP.01',
                    'I3-RIEHMTIN-14LPP.01', 'I2-RIEHMTIN-14HP.01']
    
    post_etch_df = pd.DataFrame()

    result_df = pd.read_sql(SQL,conn)

    for i, layer in enumerate(req_layers):
        each_layer_df = result_df[result_df['LAYER']==layer]
        each_layer_df['LAYER'] = replace_layer[i]
        post_etch_df = post_etch_df.append(each_layer_df)
        post_etch_df.reset_index(drop=True, inplace=True)      

    #print(post_etch_df.head(10))

    post_etch_df = post_etch_df[post_etch_df['SCRIBE'].isin(req_scribe_list)]
    post_etch_df.reset_index(drop=True, inplace=True)

    #print(SQL)

    #print(post_etch_df.head(10))

    return post_etch_df


