import io
import traceback
from pandas import read_json
from pandas.io import json
import snowflake.connector
from snowflake.connector.pandas_tools import write_pandas
import pandas
import boto3
import logging
import pandas as pd

log = logging.getLogger(__name__)

s3_client = boto3.client('s3')

data_bucket_name='gf-us-dev-mfg-ais-sagemaker-notebook'
config_file = 'etch_beam_config.json'

##file1 = 'test/etch_beam/Prediction_result_06_01_2019_to_01_01_2020.csv'
# file1 = 'Prediction_result_sample.csv'
# file2 = 'Prediction_contribution_06_01_2019_to_01_01_2020.csv'


# response1 = s3_client.get_object(Bucket=data_bucket_name, Key=file1)
# response_body1 = response1["Body"].read()
# #print(response_body)
# pred_result = pandas.read_csv(io.BytesIO(response_body1), header=0, delimiter=",", low_memory=False)
# print("Prediction Result: ",pred_result.shape)


# response2 = s3_client.get_object(Bucket=data_bucket_name, Key=file2)
# response_body2 = response2["Body"].read()
# #print(response_body)
# pred_contrbtn = pandas.read_csv(io.BytesIO(response_body2), header=0, delimiter=",", low_memory=False)
# print("Prediction Contribution: ",pred_contrbtn.shape)

# response_config = s3_client.get_object(Bucket=data_bucket_name, Key=config_file)
# response_config_body = response_config["Body"]
# jsonObject = json.loads(response_config_body.read())
# print(jsonObject)

def snowflakeReadTest():    

    jsonObject = read_json(config_file)
    sfConfig = jsonObject['snowflakeconfig']
    #print(sfConfig)
    SNOWFLAKE_SOURCE_NAME = "net.snowflake.spark.snowflake"

    try:
        py_ctx = snowflake.connector.connect(**sfConfig) 

    except Exception as e:
        traceback.print_exc()
        log.errorint("Exception occured. Retrying connection again")
        py_ctx = snowflake.connector.connect(**sfConfig) 

    cur = py_ctx.cursor()   

    # Execute a statement that will generate a result set.
    sql = "select * From ion_prediction_db.ion_pred_schema.FAB1_ION_PRED_FDC_FACT limit 10"
    cur.execute(sql)

    # Fetch the result set from the cursor and deliver it as the Pandas DataFrame.
    df = cur.fetch_pandas_all()
    #print(df.head())

    # df = pandas.DataFrame([('Mark', 10), ('Luke', 20)], columns=['NAME', 'BALANCE'])
    # #success, nchunks, nrows, _ = write_pandas(py_ctx, df, 'CUSTOMERS')
    # success, nchunks, nrows, _ = write_pandas(py_ctx, pred_result, 'PREDICTION_RESULTS')

def writePredictionResultsToSnowflakeTable(pred_result):    
    log.info("Writing prediction results to Snowflake table")
    
    writeDFtoTable(pred_result,'PREDICTION_RESULTS')

def writePredictionContributionsToSnowflakeTable(pred_contributions):
    log.info("Writing prediction contribution to Snowflake table")
    
    writeDFtoTable(pred_contributions,'PREDICTION_CONTRIBUTION')
    
def writeDFtoTable(df,table_name):
    log.info("Writing data frame to snowflake table %s",table_name)

    jsonObject = read_json(config_file)
    sfConfig = jsonObject['snowflakeconfig']
    #print(sfConfig)
    SNOWFLAKE_SOURCE_NAME = "net.snowflake.spark.snowflake"

    try:
        py_ctx = snowflake.connector.connect(**sfConfig) 

    except Exception as e:
        traceback.print_exc()
        log.errorint("Exception occured. Retrying connection again")
        py_ctx = snowflake.connector.connect(**sfConfig) 

    #cur = py_ctx.cursor()   
    #pred_result = pd.read_csv('Prediction_result.csv')
    #print(pred_result.head())
    try:
        success, nchunks, nrows, _ = write_pandas(py_ctx, df, table_name)
    except Exception as e:
        traceback.print_exc()
        log.errorint("Exception occured. Writing to table %s failed", table_name)