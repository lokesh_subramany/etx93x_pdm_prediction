import pandas as pd
import xlrd
import os
from datetime import date, datetime, time, timedelta
from pyathena import connect
from pyathena.pandas_cursor import PandasCursor
import sys
import cx_Oracle
import pandas.io.sql as psql
import logging
import xml.etree.ElementTree as et 

log = logging.getLogger(__name__)

sapphire_dsn = '(DESCRIPTION = (FAILOVER=ON)(LOAD_BALANCE=ON)(ADDRESS = (PROTOCOL = TCP)(HOST = fc8dbexa04s)(PORT = 1521))(CONNECT_DATA = (SERVICE_NAME = saphp1.gfoundries.com)))'
cfm_dsn = '(DESCRIPTION = (ADDRESS = (PROTOCOL =  TCP)(HOST=  fc8orarocfms01 )(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=cfmro.gfoundries.com)))'
gemd_dsn = '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = fc8dbexa02s)(PORT = 1521)) (CONNECT_DATA =(SERVICE_NAME = f8modsp1.gfoundries.com)))'

#global list of lot ids, this will be used for other queries
lot_list=[]

def readPTMSExportFile(ptms_file_name):   

    #ptms_file_name='ReportGeneration.xls'

    with open(ptms_file_name) as inputFile:
        result = inputFile.readlines()

    print("Read input file")

    #The xml file has an error, fixing it in the code below
    result = str(result[0])
    result = result.replace("<<Font","<Font")


    with open("CorrectedPTMS.xls",'w') as out:
        out.writelines(result)
    ptms_file_name = 'CorrectedPTMS.xls'

    df_cols=[]
    df = pd.DataFrame()

    xtree = et.parse(ptms_file_name)
    xroot = xtree.getroot() #Workbook

    #namespace = 'urn:schemas-microsoft-com:office:spreadsheet'
    for node in xroot:
        try:
            if node.attrib['{urn:schemas-microsoft-com:office:spreadsheet}Name'] == 'Module Part History':
                print('Found Module part history sheet')
                
                for count,row in enumerate(node.iter('{urn:schemas-microsoft-com:office:spreadsheet}Row')):
                    #print(row.tag,row.attrib,type(row))
                    if count == 3:
                        #print("Row number ",count)
                        for cell in row:        
                            for data in cell:                            
                                df_cols.append(data.text)
                        #print("setting cols",df_cols)
                        df = pd.DataFrame(columns=df_cols)
                        #df.columns=df_cols
                                
                    elif count > 3:
                        row_data=[]
                        for cell in row:
                            for data in cell:
                                row_data.append(data.text)
                        #print(row_data)
                        df.loc[count-3] = row_data                                                                             
                                
        except:
            continue
    #df.to_csv("PTMS_data.csv",index=False)
    print(df.head(5))

def getRFOnHoursFromXsiteInGemD(tool_chamber,from_date,to_date):
    log.info('Getting RF hours data from xsite in GEMD %s',tool_chamber)   

    toolId = tool_chamber.split('-')[0]
    chamberId= tool_chamber.split('-')[1]   
    new_to_date = from_date + timedelta(days=1)
    
    tool = toolId+chamberId
    
    db_conn = cx_Oracle.connect('DCUBE_READ', 'g3m9_d3r3ad', gemd_dsn)
    #print("setting up the db connection" )
    cursor = db_conn.cursor()
    
    SQL="""
        SELECT DISTINCT equipmentname,
        txntimestamp AS TIME,
        METERTYPE,
        NEWVALUE
        FROM xsadmin.XSITEEQPUSAGEMETERHISTORY
        WHERE txntimestamp =
        (SELECT MAX(txntimestamp)
        FROM xsadmin.XSITEEQPUSAGEMETERHISTORY
        WHERE equipmentname = :tool
        AND txntimestamp BETWEEN to_date('"""
    SQL+=from_date.strftime("%d-%m-%Y")
    SQL+="""','DD-MM-YYYY') AND to_date('"""
    SQL+=new_to_date.strftime("%d-%m-%Y")
    SQL+="""','DD-MM-YYYY'))
        AND equipmentname = :tool
        and METERTYPE = 'ETX_AnyRFOnTimeInHours'
        ORDER BY TIME DESC
    """
    log.info("RF On time hours SQL is %s: \n",SQL)
    pars={"tool":tool}
    RFHours_df = psql.read_sql(SQL, db_conn,params=pars)
    
    log.info("Rf hours for %s for duration %s,%s is  %s",tool,from_date.strftime("%m-%d-%Y"),new_to_date.strftime("%m-%d-%Y"),RFHours_df['NEWVALUE'][0])
    return(RFHours_df['NEWVALUE'][0])


def populateLotIds(toolId,start_date,end_date):
    
    log.info("Extracting lot ids from Sapphire %s,%s,%s",start_date,end_date,toolId)
    
    db = cx_Oracle.connect('DCUBE_READ', 'zBjTG85Wfnp2vmkc', sapphire_dsn)
    cursor = db.cursor()       

    lots_SQL="""
        with tech AS 
        (SELECT product_sk
        FROM product p
        WHERE ( (P.TECHNOLOGY = '012FSOI-00'
                OR P.TECHNOLOGY = '012LPPL00'
                OR P.TECHNOLOGY = '14HP'
                OR P.TECHNOLOGY = '14LPE'
                OR P.TECHNOLOGY = '14LPP'
                OR P.TECHNOLOGY = '14LPPDT'
                OR P.TECHNOLOGY = 'TEST')) ), eqptMeta AS 
        (SELECT lot_history_sk,
            eqpt_hardware_id,
            eh.eqpt_hardware_sk
        FROM SAPPHIRE.LOT_HISTORY_TO_HARDWARE eth, eqpt_and_hardware eh
        WHERE eth.eqpt_hardware_sk = eh.eqpt_hardware_sk
                AND ( (EQPT_HARDWARE_ID = :tool )) )
        SELECT DISTINCT lot_id
        FROM lot_history lh,tech,eqptMeta
        WHERE ( lh.product_sk = tech.product_sk)
            AND ( lh.lot_history_sk = eqptMeta.lot_history_sk)
            AND (lh.mfg_step_sk in
        (SELECT mfg_step_sk
        FROM mfg_process_step
        WHERE (MFG_STEP_NAME = 'E-CONDITIONING.01'
                OR MFG_STEP_NAME = 'E-CONDITIONING-1.01'
                OR MFG_STEP_NAME = 'E-DUMMY.01'
                OR MFG_STEP_NAME = 'E-ETCH-ER-2.01'
                OR MFG_STEP_NAME = 'E-PARTICLE-TEST1.01'
                OR MFG_STEP_NAME = 'E-PARTICLE-TEST2.01'
                OR MFG_STEP_NAME = 'E-PROCESS-QUAL-1.01'
                OR MFG_STEP_NAME = 'E-RIEHMTIN-PTM.01'
                OR MFG_STEP_NAME = 'E-SCRATCH-TEST-LP1.01'
                OR MFG_STEP_NAME = 'E-SCRATCH-TEST-LP2.01'
                OR MFG_STEP_NAME = 'E-SCRATCH-TEST-LP3.01'
                OR MFG_STEP_NAME = 'E-VPD.01'
                OR MFG_STEP_NAME = 'I1-RIEHMTIN-012FSOI-00.01'
                OR MFG_STEP_NAME = 'I1-RIEHMTIN-012LPPL00.01'
                OR MFG_STEP_NAME = 'I1-RIEHMTIN-14HP.01'
                OR MFG_STEP_NAME = 'I1-RIEHMTIN-14LPP.01'
                OR MFG_STEP_NAME = 'I1-RIEHMTIN-14LPPDT.01'
                OR MFG_STEP_NAME = 'I2-RIEHMSIONBSP-14LPE.01'
                OR MFG_STEP_NAME = 'I2-RIEHMTIN-012FSOI-00.01'
                OR MFG_STEP_NAME = 'I2-RIEHMTIN-012LPPL00.01'
                OR MFG_STEP_NAME = 'I2-RIEHMTIN-14HP.01'
                OR MFG_STEP_NAME = 'I2-RIEHMTIN-14LPP.01'
                OR MFG_STEP_NAME = 'I2-RIEHMTIN-14LPPDT.01'
                OR MFG_STEP_NAME = 'I3-RIEHMTIN-012LPPL00.01'
                OR MFG_STEP_NAME = 'I3-RIEHMTIN-14HP.01'
                OR MFG_STEP_NAME = 'I3-RIEHMTIN-14LPP.01'
                OR MFG_STEP_NAME = 'I3-RIEHMTIN-14LPPDT.01'
                OR MFG_STEP_NAME = 'L-EM-ETCH-STRESS.01'
                OR MFG_STEP_NAME = 'M1-RIEHMSION-012FSOI-00.01'
                OR MFG_STEP_NAME = 'M1-RIEHMSION-012LPPL00.01'
                OR MFG_STEP_NAME = 'M1-RIEHMSION-14HP.01'
                OR MFG_STEP_NAME = 'M1-RIEHMSION-14LPP.01'
                OR MFG_STEP_NAME = 'M1-RIEHMSION-14LPPDT.01'
                OR MFG_STEP_NAME = 'M2-RIEHMSION-012FSOI-00.01'
                OR MFG_STEP_NAME = 'M2-RIEHMSION-012LPPL00.01'
                OR MFG_STEP_NAME = 'M2-RIEHMSION-14HP.01'
                OR MFG_STEP_NAME = 'M2-RIEHMSION-14LPP.01'
                OR MFG_STEP_NAME = 'M2-RIEHMSION-14LPPDT.01'
                OR MFG_STEP_NAME = 'M2-RIEHMSIONBSP-14LPE.01'
                OR MFG_STEP_NAME = 'M3-RIEHMSION-012LPPL00.01'
                OR MFG_STEP_NAME = 'M3-RIEHMSION-14HP.01'
                OR MFG_STEP_NAME = 'M3-RIEHMSION-14LPP.01'
                OR MFG_STEP_NAME = 'M3-RIEHMSION-14LPPDT.01'
                OR MFG_STEP_NAME = 'S-AVT-VIBRATION-LP1.01'
                OR MFG_STEP_NAME = 'S-AVT-VIBRATION-LP2.01') ))
            AND (LH.LOT_TYPE = 'Production')
            AND DATE_STEP_STARTED BETWEEN TO_TIMESTAMP('"""
    lots_SQL+=start_date.strftime("%d-%m-%Y") #data_start_time.strftime("%Y-%m-%d %H:%M:%S")
    lots_SQL+="""', 'DD-MM-YYYY') and TO_TIMESTAMP('"""
    lots_SQL+=end_date.strftime("%d-%m-%Y")
    lots_SQL+="""', 'DD-MM-YYYY')"""
    
    #print(lots_SQL)

    params = {"tool":toolId}
    cursor.execute(lots_SQL,params)
    
    global lot_list
    lot_list =   [x[0] for x in cursor.fetchall()]
    log.info("Found lots %d",len(lot_list))
    
    #print(lot_list)
    #return [x[0] for x in cursor.fetchall()]

def getFDCKNDataFromSapphire(tool_id, date_list, module_id, data_start_time, data_end_time):
    log.info('Getting FDCKN data from Sapphire %s,%s,%s',module_id,data_start_time,data_end_time)

    populateLotIds(tool_id,data_start_time,data_end_time)
    global lot_list
    #print(lot_list)

    log.info("Executing FDCKN query")
    db_conn = cx_Oracle.connect('DCUBE_READ', 'zBjTG85Wfnp2vmkc', sapphire_dsn)
    cursor = db_conn.cursor()
    
    SQL="""
                SELECT MFG_AREA_NAME  AS MFG_AREA_NAME,
        TECHNOLOGY          AS TECHNOLOGY,
        PRODUCT_GROUP_ID    AS PRODUCT_GROUP_ID,
        PRODUCT_ID          AS PRODUCT_ID,
        LOT_ID              AS LOT_ID,
        VENDOR_SCRIBE       AS SCRIBE,
        ALIAS_WAFER_NAME    AS WAFER_NUMBER,
        CTRL_JOB            AS CTRL_JOB,
        MFG_STEP_NAME       AS STEP,
        RECIPE_MACHINE_ID   AS RECIPE_MACHINE_ID,
        RECIPE_LOGICAL_ID   AS RECIPE_LOGICAL_ID,
        FDC_CONTEXT_ID      AS FDC_CONTEXT_ID,
        RECIPE_NAME         AS RECIPE_NAME,  
        APPLICATION_NAME    AS APPLICATION_NAME,
        APPLICATION_VERSION AS APPLICATION_VERSION,
        MODEL_STATUS        AS MODEL_STATUS,
        STEP_ID             AS STEP_ID,
        START_TIME          AS START_TIME,
        END_TIME            AS END_TIME,
        FDC_RAW             AS FDC_RAW,  
        SENSOR_NAME         AS SENSOR_NAME,
        TOOL_CHAMBER        AS TOOL_CHAMBER,
        EQPT_HARDWARE_CLASS AS EQPT_HARDWARE_CLASS,
        EQPT_HARDWARE_TYPE  AS EQPT_HARDWARE_TYPE
        FROM
        (SELECT LH.MFG_AREA_NAME,
            LH.LOT_ID ,
            LH.PRODUCT_ID ,
            PG.PRODUCT_GROUP_ID ,
            WI.VENDOR_SCRIBE ,
            to_binary_double(WI.ALIAS_WAFER_NAME) AS ALIAS_WAFER_NAME ,
            LH.CTRL_JOB ,
            LH.MFG_PROCESS_MAIN_NAME ,
            LH.MFG_STEP_NAME ,
            P.TECHNOLOGY,
            (SELECT RECIPE_MACHINE_ID
            FROM RECIPE_MACHINE
            WHERE RECIPE_MACHINE_SK = LH.RECIPE_MACHINE_SK
            ) AS RECIPE_MACHINE_ID,
            (SELECT RECIPE_LOGICAL_ID
            FROM RECIPE_LOGICAL
            WHERE RECIPE_LOGICAL_SK = LH.RECIPE_LOGICAL_SK
            ) AS RECIPE_LOGICAL_ID,
            FC.FDC_CONTEXT_ID ,
            FC.RECIPE_NAME ,
            FC.CONTEXT_START_TIME AS CONTEXT_START_TIME,
            FC.CONTEXT_END_TIME   AS CONTEXT_END_TIME,
            FC.APPLICATION_NAME ,
            FC.APPLICATION_VERSION ,
            FWR.STEP_ID , -- REGEXP_REPLACE(FWR.STEP_ID, '~.*\$', '') AS RECIPE_STEP_ID,
            -- REGEXP_REPLACE(FWR.STEP_ID, '^.*~', '') AS UVA_CONTEXT_GROUP,
            CAST (FWR.START_TIME AS TIMESTAMP) AS START_TIME,
            CAST (FWR.END_TIME AS   TIMESTAMP) AS END_TIME,
            to_binary_double(FWR.FDC_RAW)      AS fdc_raw ,
            to_binary_double(FWR.FDC_KN)       AS fdc_kn ,
            FWR.FDC_UL ,
            FWR.FDC_LL ,
            FWR.FDC_EUL ,
            FWR.FDC_ELL ,
            FWR.FDC_FDI ,
            FC.MODEL_STATUS ,
            FS.SENSOR_NAME ,
            EAH.EQPT_HARDWARE_ID,
            EAH.EQPT_HARDWARE_CLASS,
            EAH.EQPT_HARDWARE_TYPE ,
            FC.CHAMBER_NAME ,
            EAH.EQPT_HARDWARE_ID
            || '_'
            || FC.CHAMBER_NAME AS TOOL_CHAMBER
        FROM SAPPHIRE.LOT_HISTORY LH,
            SAPPHIRE.FDC_CONTEXT FC,
            SAPPHIRE.FDC_WAFER_READING FWR,
            SAPPHIRE.WAFER_IDENTIFICATION WI,
            SAPPHIRE.FDC_SENSOR FS,
            SAPPHIRE.EQPT_AND_HARDWARE EAH,
            SAPPHIRE.PRODUCT P,
            SAPPHIRE.PRODUCT_GROUP PG
        WHERE lh.lot_id            in ("""
    SQL+= str(lot_list)[1:-1]
    SQL+=""")
        AND (FWR.FDC_KN           IS NOT NULL  OR FWR.FDC_RAW            IS NOT NULL)
        AND (lh.MFG_STEP_NAME      = 'E-CONDITIONING.01'
        OR lh.MFG_STEP_NAME        = 'E-CONDITIONING-1.01'
        OR lh.MFG_STEP_NAME        = 'E-DUMMY.01'
        OR lh.MFG_STEP_NAME        = 'E-ETCH-ER-2.01'
        OR lh.MFG_STEP_NAME        = 'E-PARTICLE-TEST1.01'
        OR lh.MFG_STEP_NAME        = 'E-PARTICLE-TEST2.01'
        OR lh.MFG_STEP_NAME        = 'E-PROCESS-QUAL-1.01'
        OR lh.MFG_STEP_NAME        = 'E-RIEHMTIN-PTM.01'
        OR lh.MFG_STEP_NAME        = 'E-SCRATCH-TEST-LP1.01'
        OR lh.MFG_STEP_NAME        = 'E-SCRATCH-TEST-LP2.01'
        OR lh.MFG_STEP_NAME        = 'E-SCRATCH-TEST-LP3.01'
        OR lh.MFG_STEP_NAME        = 'E-VPD.01'
        OR lh.MFG_STEP_NAME        = 'I1-RIEHMTIN-012FSOI-00.01'
        OR lh.MFG_STEP_NAME        = 'I1-RIEHMTIN-012LPPL00.01'
        OR lh.MFG_STEP_NAME        = 'I1-RIEHMTIN-14HP.01'
        OR lh.MFG_STEP_NAME        = 'I1-RIEHMTIN-14LPP.01'
        OR lh.MFG_STEP_NAME        = 'I1-RIEHMTIN-14LPPDT.01'
        OR lh.MFG_STEP_NAME        = 'I2-RIEHMSIONBSP-14LPE.01'
        OR lh.MFG_STEP_NAME        = 'I2-RIEHMTIN-012FSOI-00.01'
        OR lh.MFG_STEP_NAME        = 'I2-RIEHMTIN-012LPPL00.01'
        OR lh.MFG_STEP_NAME        = 'I2-RIEHMTIN-14HP.01'
        OR lh.MFG_STEP_NAME        = 'I2-RIEHMTIN-14LPP.01'
        OR lh.MFG_STEP_NAME        = 'I2-RIEHMTIN-14LPPDT.01'
        OR lh.MFG_STEP_NAME        = 'I3-RIEHMTIN-012LPPL00.01'
        OR lh.MFG_STEP_NAME        = 'I3-RIEHMTIN-14HP.01'
        OR lh.MFG_STEP_NAME        = 'I3-RIEHMTIN-14LPP.01'
        OR lh.MFG_STEP_NAME        = 'I3-RIEHMTIN-14LPPDT.01'
        OR lh.MFG_STEP_NAME        = 'L-EM-ETCH-STRESS.01'
        OR lh.MFG_STEP_NAME        = 'M1-RIEHMSION-012FSOI-00.01'
        OR lh.MFG_STEP_NAME        = 'M1-RIEHMSION-012LPPL00.01'
        OR lh.MFG_STEP_NAME        = 'M1-RIEHMSION-14HP.01'
        OR lh.MFG_STEP_NAME        = 'M1-RIEHMSION-14LPP.01'
        OR lh.MFG_STEP_NAME        = 'M1-RIEHMSION-14LPPDT.01'
        OR lh.MFG_STEP_NAME        = 'M2-RIEHMSION-012FSOI-00.01'
        OR lh.MFG_STEP_NAME        = 'M2-RIEHMSION-012LPPL00.01'
        OR lh.MFG_STEP_NAME        = 'M2-RIEHMSION-14HP.01'
        OR lh.MFG_STEP_NAME        = 'M2-RIEHMSION-14LPP.01'
        OR lh.MFG_STEP_NAME        = 'M2-RIEHMSION-14LPPDT.01'
        OR lh.MFG_STEP_NAME        = 'M2-RIEHMSIONBSP-14LPE.01'
        OR lh.MFG_STEP_NAME        = 'M3-RIEHMSION-012LPPL00.01'
        OR lh.MFG_STEP_NAME        = 'M3-RIEHMSION-14HP.01'
        OR lh.MFG_STEP_NAME        = 'M3-RIEHMSION-14LPP.01'
        OR lh.MFG_STEP_NAME        = 'M3-RIEHMSION-14LPPDT.01'
        OR lh.MFG_STEP_NAME        = 'S-AVT-VIBRATION-LP1.01'
        OR lh.MFG_STEP_NAME        = 'S-AVT-VIBRATION-LP2.01')
        AND FC.LOT_SK              = LH.LOT_SK
        AND FC.CTRL_JOB            = LH.CTRL_JOB
        AND P.PRODUCT_SK           = LH.PRODUCT_SK
        AND PG.PRODUCT_GROUP_SK    = P.PRODUCT_GROUP_SK
        AND FWR.LOT_SK             = FC.LOT_SK
        AND FWR.FDC_CONTEXT_SK     = FC.FDC_CONTEXT_SK
        AND FC.WAFER_ID_SK         = WI.WAFER_ID_SK
        AND FS.FDC_SENSOR_SK       = FWR.FDC_SENSOR_SK
        AND EAH.EQPT_HARDWARE_SK   = FC.EQPT_HARDWARE_SK --AND T.TECHNOLOGY_SK = PG.TECHNOLOGY_SK
        AND ( (FC.APPLICATION_NAME = 'FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_SOH_ESC'
        OR FC.APPLICATION_NAME     = 'FD_ETC_LAM_METALM_ETX2930X_EQP'
        OR FC.APPLICATION_NAME     = 'FD_ETC_LAM_METALM_ETX2930X_COND_PR'
        OR FC.APPLICATION_NAME     = 'FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO'
        OR FC.APPLICATION_NAME     = 'FD_ETC_LAM_METALM_ETX2930X_QUAL'
        OR FC.APPLICATION_NAME     = 'pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO'
        OR FC.APPLICATION_NAME     = 'GHG_ETC_LAM_METAL_M_PM_ETX2930X'))
        AND ( (FWR.STEP_ID         = 'SION_ME~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'CustomEquation~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_SOH_ESC~P-14NM-MXIX-ALL'
        OR FWR.STEP_ID             = 'DataQuality~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_SOH_ESC~P-14NM-MXIX-ALL'
        OR FWR.STEP_ID             = 'ASH2~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-MX'
        OR FWR.STEP_ID             = 'DataQuality~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-MX'
        OR FWR.STEP_ID             = 'ASH2~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-MX'
        OR FWR.STEP_ID             = 'SION2_ME_LISTEN~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'SION_ME~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'ASH1~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-M1'
        OR FWR.STEP_ID             = 'CustomEquation~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-MX'
        OR FWR.STEP_ID             = 'SION_ME~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-MX'
        OR FWR.STEP_ID             = 'ASH1~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'SION2_ME_LISTEN~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-M1'
        OR FWR.STEP_ID             = 'DataQuality~FD_ETC_LAM_METALM_ETX2930X_QUAL~Q-ER-SOH'
        OR FWR.STEP_ID             = 'SION_ME~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-MX'
        OR FWR.STEP_ID             = 'DataQuality~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'ASH2~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-M1'
        OR FWR.STEP_ID             = 'ASH1~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-MX'
        OR FWR.STEP_ID             = 'ETCH_RATE~FD_ETC_LAM_METALM_ETX2930X_QUAL~Q-ER-SION'
        OR FWR.STEP_ID             = 'ASH2~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'SION2_ME_LISTEN~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-M1'
        OR FWR.STEP_ID             = 'ASH1~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-MX'
        OR FWR.STEP_ID             = 'ETCH_RATE~FD_ETC_LAM_METALM_ETX2930X_QUAL~Q-ER-SOH'
        OR FWR.STEP_ID             = 'SION2_ME_LISTEN~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'CustomEquation~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'BARC_ME~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-MX'
        OR FWR.STEP_ID             = 'FLASH~FD_ETC_LAM_METALM_ETX2930X_COND_PR~Q-COND-RESIST'
        OR FWR.STEP_ID             = 'TIN_ME~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'BARC_ME~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'BARC_ME~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-MX'
        OR FWR.STEP_ID             = 'Full_Run_CONTROL~FD_ETC_LAM_METALM_ETX2930X_EQP~P-EQP-ALL'
        OR FWR.STEP_ID             = 'BARC_ME~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'Full_Run_LISTEN~FD_ETC_LAM_METALM_ETX2930X_EQP~P-EQP-ALL'
        OR FWR.STEP_ID             = 'SOH~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'SOH~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-M1'
        OR FWR.STEP_ID             = 'CustomEquation~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-M1'
        OR FWR.STEP_ID             = 'ASH1~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-M1'
        OR FWR.STEP_ID             = 'DataQuality~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-MX'
        OR FWR.STEP_ID             = 'STRIP~FD_ETC_LAM_METALM_ETX2930X_COND_PR~Q-COND-RESIST'
        OR FWR.STEP_ID             = 'CustomEquation~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'SOH~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'SOH~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-M1'
        OR FWR.STEP_ID             = 'CustomEquation~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-MX'
        OR FWR.STEP_ID             = 'BARC~FD_ETC_LAM_METALM_ETX2930X_COND_PR~Q-COND-RESIST'
        OR FWR.STEP_ID             = 'ASH2~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'SION_ME~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-M1'
        OR FWR.STEP_ID             = 'TRIM~FD_ETC_LAM_METALM_ETX2930X_COND_PR~Q-COND-RESIST'
        OR FWR.STEP_ID             = 'ASH2~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'SION_ME~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'SOH~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-MX'
        OR FWR.STEP_ID             = 'TIN~FD_ETC_LAM_METALM_ETX2930X_COND_PR~Q-COND-RESIST'
        OR FWR.STEP_ID             = 'TIN_ME~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'DataQuality~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'DataQuality~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'ASH1_LISTEN~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-MX'
        OR FWR.STEP_ID             = 'SOH~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'ASH1~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'ASH1_LISTEN~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'CustomEquation~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'BARC_ME~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'BARC_ME~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-M1'
        OR FWR.STEP_ID             = 'DataQuality~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-M1'
        OR FWR.STEP_ID             = 'DataQuality~FD_ETC_LAM_METALM_ETX2930X_QUAL~Q-ER-SION'
        OR FWR.STEP_ID             = 'SOH_LISTEN~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_SOH_ESC~P-14NM-MXIX-ALL'
        OR FWR.STEP_ID             = 'SOH~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'SION2_ME_LISTEN~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'CustomEquation~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-M1'
        OR FWR.STEP_ID             = 'ASH1_LISTEN~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'ASH1_LISTEN~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-M1'
        OR FWR.STEP_ID             = 'BARC_ME~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-M1'
        OR FWR.STEP_ID             = 'DataQuality~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-M1'
        OR FWR.STEP_ID             = 'SION_ME~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-M1'
        OR FWR.STEP_ID             = 'ASH2~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-M1'
        OR FWR.STEP_ID             = 'SOH~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-MX'
        OR FWR.STEP_ID             = 'DataQuality~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'SION2_ME_LISTEN~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-MX'
        OR FWR.STEP_ID             = 'CustomEquation~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'SION_ME~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'TIN_ME~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'TIN_ME~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'Window_1~GHG_ETC_LAM_METAL_M_PM_ETX2930X~Default'
        OR FWR.STEP_ID             = 'ASH2~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'BARC_ME~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'SION2_ME_LISTEN~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-MX'
        OR FWR.STEP_ID             = 'ASH1~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-I1'
        OR FWR.STEP_ID             = 'SION2_ME_LISTEN~FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'ASH1~pec_FD_ETC_LAM_METALM_ETX2930X_14NM_MXIX_HMO~P-14NM-IX'
        OR FWR.STEP_ID             = 'SION~FD_ETC_LAM_METALM_ETX2930X_COND_PR~Q-COND-RESIST'))
        AND ( (FS.SENSOR_NAME      = 'PGC_CF4_mfc__Gas_CF4_Flow_Range'
        OR FS.SENSOR_NAME          = 'TemperatureControl_HeatedWindow_TCPWindowHeater__TemperatureSensorReading_Mean'
        OR FS.SENSOR_NAME          = 'PGC_ThrottleValve_Controller__PositionSensorReading_MeanH'
        OR FS.SENSOR_NAME          = 'Bias__RFGenTemperatureAIReading_Range'
        OR FS.SENSOR_NAME          = 'PGC_SF6_mfc__Gas_SF6_Flow_Area'
        OR FS.SENSOR_NAME          = 'OES__IB4Value_Mean'
        OR FS.SENSOR_NAME          = 'PGC_CHF3_mfc__Gas_CHF3_Flow_Area'
        OR FS.SENSOR_NAME          = 'PGC_Ar_mfc__Gas_Ar_Flow_Range'
        OR FS.SENSOR_NAME          = 'PGC_N2_mfc__Gas_N2_Flow_Range'
        OR FS.SENSOR_NAME          = 'TCP__RFGenTemperatureAIReading_Range'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_MidOuterESC__temperatureSensorAdjusted_Min'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_OuterESC__temperatureSensorAdjusted_Min'
        OR FS.SENSOR_NAME          = 'PGC_ThrottleValve_Controller__PositionSensorReading_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_Chamber_RearRight__TemperatureSensorReading_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_MidInnerESC__temperatureSensorAdjusted_PointCountBelow'
        OR FS.SENSOR_NAME          = 'TemperatureControl_Chamber_FrontLeft__TemperatureSensorReading_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_InnerESC__temperatureSensorAdjusted_Stdev'
        OR FS.SENSOR_NAME          = 'PGC_ForelineManometer__LowManometerAdjustedPressure_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_Chamber_FrontRight__TemperatureSensorReading_Mean'
        OR FS.SENSOR_NAME          = 'Bias__RFGenForwardPowerAIReading_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_MidInnerESC__DutyCycleSensorReading_Mean'
        OR FS.SENSOR_NAME          = 'PGC_N2_mfc__Gas_N2_Flow_Mean'
        OR FS.SENSOR_NAME          = 'PGC_SF6_mfc__Gas_SF6_Flow_Range'
        OR FS.SENSOR_NAME          = 'PGC_High_Cl2_mfc__Gas_High_Cl2_Flow_Mean'
        OR FS.SENSOR_NAME          = 'PGC_GasLineHeater2Temperature__sensorReading_Mean'
        OR FS.SENSOR_NAME          = 'PGC_ChamberPressure_HighManometer__AdjustedPressure_Mean'
        OR FS.SENSOR_NAME          = 'PGC_CHF3_mfc__Gas_CHF3_Flow_Mean'
        OR FS.SENSOR_NAME          = 'Chuck__HeBacksidePressureReading_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_OuterESC__temperatureSensorAdjusted_PointCountBelow'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_MidOuterESC__TemperatureSensorReading_Stdev'
        OR FS.SENSOR_NAME          = 'PGC_H2_mfc__Gas_H2_Flow_Range'
        OR FS.SENSOR_NAME          = 'PGC_SiCl4_mfc__Gas_SiCl4_Flow_Range'
        OR FS.SENSOR_NAME          = 'CustomEquation_AutoUDT_CH_ESCTemp_PointCount'
        OR FS.SENSOR_NAME          = 'Chuck__HeBacksideFlowReading_Range'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_MidOuterESC__temperatureSensorAdjusted_Mean'
        OR FS.SENSOR_NAME          = 'TCP__RFGenTemperatureAIReading_Mean'
        OR FS.SENSOR_NAME          = 'TCP_Match_C1Cap__PositionAIReading_Mean'
        OR FS.SENSOR_NAME          = 'TCP_Match__InnerCoilCurrentAIReading_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_OuterESC__TemperatureSensorReading_Mean'
        OR FS.SENSOR_NAME          = 'PGC_Ar_mfc__Gas_Ar_Flow_Mean'
        OR FS.SENSOR_NAME          = 'TCP__RFGenReflectedPowerAIReading_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_InnerESC__DutyCycleSensorReading_Mean'
        OR FS.SENSOR_NAME          = 'PGC_GasLineHeater1Temperature__sensorReading_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_ESC__TargetTemperatureSensorReading_Mean'
        OR FS.SENSOR_NAME          = 'OES__IB2Value_Mean'
        OR FS.SENSOR_NAME          = 'TCP__RFGenReflectedPowerAIReading_Range'
        OR FS.SENSOR_NAME          = 'PGC_ChamberPressure_LowManometer__AdjustedPressure_Mean'
        OR FS.SENSOR_NAME          = 'PGC_ThrottleValve_Controller__PositionSensorReading_Stdev'
        OR FS.SENSOR_NAME          = 'OES__IB1Value_Mean'
        OR FS.SENSOR_NAME          = 'Bias__RFVoltageProbeAIReading_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_InnerESC__temperatureSensorAdjusted_Mean'
        OR FS.SENSOR_NAME          = 'PGC_SF6_mfc__Gas_SF6_Flow_Mean'
        OR FS.SENSOR_NAME          = 'PGC_CH4_mfc__Gas_CH4_Flow_Mean'
        OR FS.SENSOR_NAME          = 'PGC_H2_mfc__Gas_H2_Flow_Mean'
        OR FS.SENSOR_NAME          = 'PGC_NF3_mfc__Gas_NF3_Flow_Mean'
        OR FS.SENSOR_NAME          = 'PGC_CH2F2_mfc__Gas_CH2F2_Flow_Mean'
        OR FS.SENSOR_NAME          = 'PGC_CH2F2_mfc__Gas_CH2F2_Flow_Range'
        OR FS.SENSOR_NAME          = 'PGC_CH4_mfc__Gas_CH4_Flow_Range'
        OR FS.SENSOR_NAME          = 'CustomEquation_AutoUDT_CH_TVPosition'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_MidOuterESC__temperatureSensorAdjusted_PointCountBelow'
        OR FS.SENSOR_NAME          = 'TemperatureControl_Chiller__Temperature1AIReading_Range'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_MidInnerESC__TemperatureSensorReading_Stdev'
        OR FS.SENSOR_NAME          = 'TCP__RFGenForwardPowerAIReading_Mean'
        OR FS.SENSOR_NAME          = 'CustomEquation_AutoUDT_CH_BiasRFRef'
        OR FS.SENSOR_NAME          = 'PGC_Low_O2_mfc__Gas_Low_O2_Flow_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_InnerESC__temperatureSensorAdjusted_PointCountBelow'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_MidOuterESC__DutyCycleSensorReading_Mean'
        OR FS.SENSOR_NAME          = 'Chuck__ESCTotalVoltageReading_Range'
        OR FS.SENSOR_NAME          = 'TemperatureControl_HeatedWindow_TCRHeater__TemperatureSensorReading_Mean'
        OR FS.SENSOR_NAME          = 'TCP_Match__OuterCoilCurrentAIReading_Mean'
        OR FS.SENSOR_NAME          = 'PGC_NF3_mfc__Gas_NF3_Flow_Area'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_OuterESC__TemperatureSensorReading_Stdev'
        OR FS.SENSOR_NAME          = 'PGC_High_O2_mfc__Gas_High_O2_Flow_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_InnerESC__temperatureSensorAdjusted_Min'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_InnerESC__TemperatureSensorReading_Mean'
        OR FS.SENSOR_NAME          = 'PGC_BCl3_mfc__Gas_BCl3_Flow_Mean'
        OR FS.SENSOR_NAME          = 'Bias__RFGenTemperatureAIReading_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_MidInnerESC__temperatureSensorAdjusted_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_InnerESC__TemperatureSensorReading_Stdev'
        OR FS.SENSOR_NAME          = 'PGC_NF3_mfc__Gas_NF3_Flow_Range'
        OR FS.SENSOR_NAME          = 'PGC_Low_Cl2_mfc__Gas_Low_Cl2_Flow_Range'
        OR FS.SENSOR_NAME          = 'OES__IB3Value_Mean'
        OR FS.SENSOR_NAME          = 'Chuck__ESCBiasVoltageReading_Mean'
        OR FS.SENSOR_NAME          = 'PGC_SiCl4_mfc__Gas_SiCl4_Flow_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_OuterESC__temperatureSensorAdjusted_Stdev'
        OR FS.SENSOR_NAME          = 'Chuck__HeBacksideFlowReading_Mean'
        OR FS.SENSOR_NAME          = 'Bias__RFGenReflectedPowerAIReading_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_Chiller__Temperature1AIReading_Mean'
        OR FS.SENSOR_NAME          = 'RecipeStepNumber_Count'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_MidInnerESC__temperatureSensorAdjusted_Stdev'
        OR FS.SENSOR_NAME          = 'PGC_Low_O2_mfc__Gas_Low_O2_Flow_Range'
        OR FS.SENSOR_NAME          = 'PGC_High_O2_mfc__Gas_High_O2_Flow_Range'
        OR FS.SENSOR_NAME          = 'PGC_CF4_mfc__Gas_CF4_Flow_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_Chamber_RearLeft__TemperatureSensorReading_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_MidOuterESC__TemperatureSensorReading_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_OuterESC__DutyCycleSensorReading_Mean'
        OR FS.SENSOR_NAME          = 'Chuck__ESCTotalVoltageReading_Mean'
        OR FS.SENSOR_NAME          = 'PGC_High_Cl2_mfc__Gas_High_Cl2_Flow_Range'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_MidInnerESC__temperatureSensorAdjusted_Min'
        OR FS.SENSOR_NAME          = 'TemperatureControl_ESC__TargetFlowSensorReading_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_MidOuterESC__temperatureSensorAdjusted_Stdev'
        OR FS.SENSOR_NAME          = 'CustomEquation_AutoUDT_CH_ESCTemp_SOH'
        OR FS.SENSOR_NAME          = 'PGC_BCl3_mfc__Gas_BCl3_Flow_Range'
        OR FS.SENSOR_NAME          = 'Bias__RFGenReflectedPowerAIReading_Range'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_MidInnerESC__TemperatureSensorReading_Mean'
        OR FS.SENSOR_NAME          = 'PGC_CF4_mfc__Gas_CF4_Flow_Area'
        OR FS.SENSOR_NAME          = 'PGC__PressureReading_Mean'
        OR FS.SENSOR_NAME          = 'TemperatureControl_TunableESC_OuterESC__temperatureSensorAdjusted_Mean'
        OR FS.SENSOR_NAME          = 'PGC_Low_Cl2_mfc__Gas_Low_Cl2_Flow_Mean'
        OR FS.SENSOR_NAME          = 'PGC_HBr_mfc__Gas_HBr_Flow_Range'
        OR FS.SENSOR_NAME          = 'PGC_CHF3_mfc__Gas_CHF3_Flow_Range'
        OR FS.SENSOR_NAME          = 'PGC_HBr_mfc__Gas_HBr_Flow_Mean'))
        )
        WHERE tool_chamber = :tool """
        #and start_time between :start and :end"""
        #to_timestamp(:start,'DD-MM-YYYY') and to_timestamp(:end,'DD-MM-YYYY')
    SQL+=""" and start_time between to_timestamp('"""
    SQL+=data_start_time.strftime("%d-%m-%Y") 
    SQL+="""','DD-MM-YYYY') and to_timestamp('"""
    SQL+=data_end_time.strftime("%d-%m-%Y")
    SQL+="""','DD-MM-YYYY')  """       
    
    query_start = datetime.now()
    
    #print(SQL)
    #pars={"tool":module_id,"start":datetime.strptime(data_start_time, '%d-%m-%Y').date(),"end":datetime.strptime(data_end_time, '%d-%m-%Y').date()}
    pars={"tool":module_id}
    #FDC_df = pd.read_sql(SQL,conn)
        
    FDC_df = psql.read_sql(SQL, db_conn,params=pars)
    
    log.info("No of Records in FDC_KN Data between %s & %s is: %d",data_start_time.strftime("%d-%m-%Y"),data_end_time.strftime("%d-%m-%Y"),FDC_df.shape[0])
    
    query_end = datetime.now()
    log.info("FDC query time is %s",query_end - query_start)

    if FDC_df.shape[0] == 0:
        log.critical("FDC Data is empty, Terminating program")        
        raise Exception("FDC data is empty")
        #sys.exit()
    
    return FDC_df

def getInlineSpaceDataFromGEMD(tool_chamber,req_scribe_list,from_date,to_date):
    log.info('Getting Inline space data from GEMD %s',tool_chamber)   

    toolId = tool_chamber.split('-')[0]
    chamberId= tool_chamber.split('-')[1]

    #global lot_list
    #print(lot_list)
    
    db_conn = cx_Oracle.connect('DCUBE_READ', 'g3m9_d3r3ad', gemd_dsn)
    #print("setting up the db connection")
    cursor = db_conn.cursor()

    SQL="""
        SELECT DISTINCT S.EXVAL_00 AS LOTID,
        S.EXVAL_01               AS SCRIBE,
        S.EXVAL_09               AS PROCESS_STEP,        
        S.PARAMETER_NAME,
        S.EXVAL_02 AS RECIPE,
        S.EXVAL_03 AS TOOL,
        S.EXVAL_04 AS CHAMBER,
        RAW_VALUE,
        S.SPEC_LOWER     AS LSL,
        L3.EXT_MV_LCL    AS LCL,
        S.SPEC_TARGET    AS TGT,
        L3.EXT_MV_CENTER AS CENTER,
        L3.EXT_MV_UCL    AS UCL,
        S.SPEC_UPPER     AS USL,
        L3.CH_STATE      AS CHART_STATE
        FROM SPACE.T_EXT_SAMPLES S ,
        SPACE.T_EXT_SAMPLES_RAW R ,
        SPACE.T_EXT_SAMPLES_CALC L1 ,
        SPACE.T_CKC_SPC_DATA L3
        WHERE S.SAMPLE_ID     = R.SAMPLE_ID
        AND L3.CH_ID          =L1.CH_ID
        AND L3.CH_STATE       ='Online'
        AND S.SPC_COM_INTERN IS NULL
        AND S.SAMPLE_ID       =L1.SAMPLE_ID
        AND L3.CH_STATE      IS NOT NULL
        /******************************************PLEASE ADD THE APPROPRIATE PD INFORMATION ************************************************/
        AND S.EXVAL_19 IN ('e1V164-M1-MTRFCDPRIE-14LPP-A0.01
        ','e1V165-M1-MTRFCDPRIE-14LPP-A0.01
        ','e1V509-I2-MTRFCDRIEHMTIN-14LPP-A4.01
        ','e1V509-I3-MTRFCDRIEHMTIN-14LPP-A6.01
        ','e1V509-M2-MTRFCDPRIE-14LPP-A3.01
        ','e1V509-M3-MTRFCDPRIE-14LPP-A5.01
        ','e1W093-I1-MTRFCDRIEHMTIN-14LPP-A1.01
        ','e1W093-I2-MTRFCDRIEHMTIN-14LPP-A4.01
        ','e1W093-I3-MTRFCDRIEHMTIN-14LPP-A6.01
        ','e1W093-M1-MTRFCDPRIE-14LPP-A0.01
        ','e1W093-M2-MTRFCDPRIE-14LPP-A3.01
        ','e1W093-M3-MTRFCDPRIE-14LPP-A5.01
        ','e1W174-I2-MTRFCDRIEHMTIN-14HP-A4.01
        ','e1W174-I3-MTRFCDRIEHMTIN-14HP-A6.01
        ','e1W174-M2-MTRFCDPRIE-14HP-A3.01
        ','e1W174-M3-MTRFCDPRIE-14HP-A5.01
        ','e1W338-M2-MTRFCDPRIE-14LPP-A1.01
        ','e1W394-I2-MTRFCDRIEHMTIN-14LPP-A1.01
        ','e1X116-I1-MTRFCDRIEHMTIN-14LPP-AB.01
        ','e1X116-M1-MTRFCDPRIE-14LPP-AA.01
        ','e1Y129-I3-MTRFCDRIEHMTIN-14LPP-A6.01
        ','e1Y129-M3-MTRFCDPRIE-14LPP-A5.01
        ','I1-MTRFCDRIEHMTIN-012FSOI-00.01
        ','I1-MTRFCDRIEHMTIN-012LPPL00.01
        ','I1-MTRFCDRIEHMTIN-14HP.01
        ','I1-MTRFCDRIEHMTIN-14LPP.01
        ','I2-MTRFCDRIEHMTIN-012FSOI-00.01
        ','I2-MTRFCDRIEHMTIN-012LPPL00.01
        ','I2-MTRFCDRIEHMTIN-14HP.01
        ','I2-MTRFCDRIEHMTIN-14LPP.01
        ','I3-MTRFCDRIEHMTIN-012LPPL00.01
        ','I3-MTRFCDRIEHMTIN-14HP.01
        ','I3-MTRFCDRIEHMTIN-14LPP.01
        ','LI1-MTRFCD-012FSOI-00.01
        ','LI2-MTRFCD-012FSOI-00.01
        ','LI2-MTRFCDWCMP-012FSOI-00.01
        ','M1-MTRFCDPRIE-012FSOI-00.01
        ','M1-MTRFCDPRIE-012LPPL00.01
        ','M1-MTRFCDPRIE-14HP.01
        ','M1-MTRFCDPRIE-14LPP.01
        ','M2-MTRFCDPRIE-012FSOI-00.01
        ','M2-MTRFCDPRIE-012LPPL00.01
        ','M2-MTRFCDPRIE-14HP.01
        ','M2-MTRFCDPRIE-14LPP.01
        ','M3-MTRFCDPRIE-012LPPL00.01
        ','M3-MTRFCDPRIE-14HP.01
        ','M3-MTRFCDPRIE-14LPP.01')
        AND s.sample_date BETWEEN to_date('"""
    SQL+=from_date.strftime("%m-%d-%Y")
    SQL+="""','mm-dd-yyyy') AND to_date('"""
    SQL+=to_date.strftime("%m-%d-%Y")
    SQL+="""','mm-dd-yyyy')
        AND S.EXVAL_03 = :tool
        AND s.EXVAL_04 = :chamber"""

    pars={"tool":toolId,"chamber":chamberId}
    inline_df = psql.read_sql(SQL, db_conn,params=pars)
    log.info("No of Records in inline space Data between %s & %s: %d",from_date.strftime("%m-%d-%Y"),to_date.strftime("%m-%d-%Y"),inline_df.shape[0])
    
    return inline_df

def getWaferHistoryDataFromSapphire(tool_chamber,req_scribe_list,from_date,to_date):
    log.info('Getting wafer history from Sapphire %s',tool_chamber)
    toolId = tool_chamber.split('-')[0]

    global lot_list
    #print(lot_list)
    
    db_conn = cx_Oracle.connect('DCUBE_READ', 'zBjTG85Wfnp2vmkc', sapphire_dsn)
    cursor = db_conn.cursor()

    SQL="""   
        SELECT
                        TECHNOLOGY as TECHNOLOGY,PRODUCT_GROUP_ID as PRODUCT_GROUP_ID,PRODUCT_ID as PRODUCT_ID,LOT_ID as LOT_ID,LOT_FAMILY_ID as INIT_FAB_LOT,MFG_STEP_NAME as STEP,DATE_STEP_STARTED as DATE_STEP_STARTED,DATE_STEP_FINISHED as DATE_STEP_FINISHED,VENDOR_SCRIBE as SCRIBE,ALIAS_WAFER_NAME as WAFER_NUMBER,EQPT_HARDWARE_ID as TOOL,TOOL_LOCATION as TOOL_LOCATION,EQPT_MAINT_GROUP as EQPT_MAINT_GROUP,LOCATION as LOCATION,LOCATION_TYPE as LOCATION_TYPE,ENTER_TS as ENTER_TS,EXIT_TS as EXIT_TS,LOC_SEQ as LOC_SEQ,CTRL_JOB as CTRL_JOB,DCR_RECIPE_ID as DCR_RECIPE_ID,EXPERIMENTORIGINALPD as EXPERIMENTORIGINALPD
                        FROM
                          (
                            SELECT distinct
                                lh.mfg_area_name,
                                lh.mfg_route_name,                                
                                P.TECHNOLOGY,
                                pg.product_group_id ,
                                  lh.product_id, 
                                     SUBSTR (LH.MFG_PROCESS_MAIN_NAME,
                                             1,
                                             LEAST (INSTR (LH.MFG_PROCESS_MAIN_NAME, '.') - 1, 64))             
                                     as PROCESS,
                                DECODE ( substr(lh.MFG_PROCESS_MAIN_NAME ,1,1),'e', p.MFG_PROCESS_MAIN_NAME,'NA') as base_process,
                                lh.mfg_step_name,
                                lh.oper_num,
                                L.LOT_ID as LOT_FAMILY_ID,
                                lh.lot_type,
                                child_lot.sub_lot_type,
                                lh.owner_code,
                                lh.customer_code,    
                                lh.start_operator_id,
                                lh.photo_layer,          
                                lh.date_step_started,
                                lh.date_step_finished,
                                lh.date_process_started, 
                                lh.date_process_finished, 
                                lh.mfg_process_main_name, 
                                lh.oper_pass_count,                                    
                                mpstg.mfg_process_stage_id,
                                cast(wi.alias_wafer_name as number(3)) as alias_wafer_name,
                                wi.vendor_scribe,
                                eah.eqpt_hardware_id,
                                eah.eqpt_hardware_type, 
                                eah.supplier_id, 
                                eah.eqpt_maint_group,
                                wl.location,          
                                wl.location_type,
                                wl.enter_ts AS enter_ts,
                                wl.exit_ts AS exit_ts,
                                eah.eqpt_hardware_id || '_' || wl.location as tool_location,
                                cast(  CASE
                                     WHEN stitched_flag IS NOT NULL OR wl.enter_ts IS NOT NULL
                                     THEN
                                        dense_rank ()
                                           over (partition by LH.LOT_HISTORY_SK,
                                                              wl.dcr_context_sk,
                                                              lh.ctrl_job,
                                                              l.lot_id,
                                                              wl.wafer_id_sk,
                                                              lh.mfg_area_name,
                                                              lh.mfg_step_name,
                                                              wl.eqpt_hardware_sk,
                                                              wl.location_type
                                                 ORDER BY wl.enter_ts ASC NULLS LAST)
                                     ELSE
                                        NULL
                                  END as int)
                                     AS loc_seq,
                                  lh.ctrl_job,
                                  drr.dcr_recipe_id,
                                  nvl(usk.udata_value, lh.mfg_step_name) AS ExperimentOriginalPD,
                                  lh.lot_id,          
                                  --COALESCE(wl.ENTER_TS,wl.EXIT_TS) AS Enter_Exit_ts,
                                  wl.controljob_id,
                                  lot_history_sk,
                                  eah.eqpt_hardware_sk
                                  ,RANK() OVER (PARTITION BY nvl(usk.udata_value, lh.mfg_step_name), wi.vendor_scribe order by DATE_STEP_STARTED desc ) as Rank
                             from 
                                  sapphire.lot l,
                                  sapphire.lot child_lot,
                                  sapphire.mfg_process_step mps,
                                  sapphire.lot_history lh,                                  
                                  sapphire.wafer_location wl,
                                  sapphire.wafer_identification wi,
                                  sapphire.eqpt_and_hardware eah,
                                  sapphire.mfg_process_stage mpstg,
                                  sapphire.dcr_recipe drr,
                                  sapphire.DCR_LOT DRL,
                                  sapphire.PRODUCT P,
                                  sapphire.PRODUCT_GROUP PG,
                                  sapphire.MFG_PROCESS_STEP_UDATA USK,
                                  sapphire.initial_fab_lot ifl
                                  
                            WHERE     lh.lot_sk = ifl.lot_sk
                                  AND lh.lot_sk = child_lot.lot_sk
                                  AND ifl.init_fab_lot_sk = l.lot_sk
                                  AND lh.mfg_step_sk = mps.mfg_step_sk
                                  AND wl.wafer_id_sk = wi.wafer_id_sk
                                  AND wl.lot_sk = lh.lot_sk
                                  AND wl.controljob_id = lh.ctrl_job
                                  AND wl.eqpt_hardware_sk = eah.eqpt_hardware_sk
                                  AND mpstg.mfg_process_stage_sk(+) = lh.mfg_process_stage_sk
                                  AND drr.dcr_recipe_sk = drl.dcr_recipe_sk
                                  AND wl.lot_sk = drl.lot_sk
                                  AND wl.dcr_context_sk = drl.dcr_context_sk
                                  AND drl.controljob_id = wl.controljob_id
                                  AND ifl.lot_sk = drl.lot_sk
                                  and DRL.MFG_STEP_SK = LH.MFG_STEP_SK
                                  and USK.MFG_STEP_SK(+) = LH.MFG_STEP_SK
                                  and usk.UDATA_NAME(+) = 'ExperimentOriginalPD'
                                  AND P.PRODUCT_SK(+) = LH.PRODUCT_SK
                                  and PG.PRODUCT_GROUP_SK(+) = P.PRODUCT_GROUP_SK
                                  and lh.LOT_ID in ("""
    SQL+= str(lot_list)[1:-1]
    SQL+=""")
        and  ( wl.location_type = 'COMPONENT' OR  wl.location_type = 'FOUP' OR  wl.location_type = 'PROCESSINGAREA' OR  wl.location_type = 'SUBPROCESSINGAREA') -- Location Types
                                   -- Scope
                               
					AND  (lh.mfg_step_name = 'E-CONDITIONING.01' OR lh.mfg_step_name = 'E-CONDITIONING-1.01' OR lh.mfg_step_name = 'E-DUMMY.01' OR lh.mfg_step_name = 'E-ETCH-ER-2.01' OR lh.mfg_step_name = 'E-PARTICLE-TEST1.01' OR lh.mfg_step_name = 'E-PARTICLE-TEST2.01' OR lh.mfg_step_name = 'E-PROCESS-QUAL-1.01' OR lh.mfg_step_name = 'E-RIEHMTIN-PTM.01' OR lh.mfg_step_name = 'E-SCRATCH-TEST-LP1.01' OR lh.mfg_step_name = 'E-SCRATCH-TEST-LP2.01' OR lh.mfg_step_name = 'E-SCRATCH-TEST-LP3.01' OR lh.mfg_step_name = 'E-VPD.01' OR lh.mfg_step_name = 'I1-RIEHMTIN-012FSOI-00.01' OR lh.mfg_step_name = 'I1-RIEHMTIN-012LPPL00.01' OR lh.mfg_step_name = 'I1-RIEHMTIN-14HP.01' OR lh.mfg_step_name = 'I1-RIEHMTIN-14LPP.01' OR lh.mfg_step_name = 'I1-RIEHMTIN-14LPPDT.01' OR lh.mfg_step_name = 'I2-RIEHMSIONBSP-14LPE.01' OR lh.mfg_step_name = 'I2-RIEHMTIN-012FSOI-00.01' OR lh.mfg_step_name = 'I2-RIEHMTIN-012LPPL00.01' OR lh.mfg_step_name = 'I2-RIEHMTIN-14HP.01' OR lh.mfg_step_name = 'I2-RIEHMTIN-14LPP.01' OR lh.mfg_step_name = 'I2-RIEHMTIN-14LPPDT.01' OR lh.mfg_step_name = 'I3-RIEHMTIN-012LPPL00.01' OR lh.mfg_step_name = 'I3-RIEHMTIN-14HP.01' OR lh.mfg_step_name = 'I3-RIEHMTIN-14LPP.01' OR lh.mfg_step_name = 'I3-RIEHMTIN-14LPPDT.01' OR lh.mfg_step_name = 'L-EM-ETCH-STRESS.01' OR lh.mfg_step_name = 'M1-RIEHMSION-012FSOI-00.01' OR lh.mfg_step_name = 'M1-RIEHMSION-012LPPL00.01' OR lh.mfg_step_name = 'M1-RIEHMSION-14HP.01' OR lh.mfg_step_name = 'M1-RIEHMSION-14LPP.01' OR lh.mfg_step_name = 'M1-RIEHMSION-14LPPDT.01' OR lh.mfg_step_name = 'M2-RIEHMSION-012FSOI-00.01' OR lh.mfg_step_name = 'M2-RIEHMSION-012LPPL00.01' OR lh.mfg_step_name = 'M2-RIEHMSION-14HP.01' OR lh.mfg_step_name = 'M2-RIEHMSION-14LPP.01' OR lh.mfg_step_name = 'M2-RIEHMSION-14LPPDT.01' OR lh.mfg_step_name = 'M2-RIEHMSIONBSP-14LPE.01' OR lh.mfg_step_name = 'M3-RIEHMSION-012LPPL00.01' OR lh.mfg_step_name = 'M3-RIEHMSION-14HP.01' OR lh.mfg_step_name = 'M3-RIEHMSION-14LPP.01' OR lh.mfg_step_name = 'M3-RIEHMSION-14LPPDT.01' OR lh.mfg_step_name = 'S-AVT-VIBRATION-LP1.01' OR lh.mfg_step_name = 'S-AVT-VIBRATION-LP2.01')) Where RANK = 1 
          and date_step_started between to_date('"""
    SQL+=from_date.strftime("%m-%d-%Y")
    SQL+="""','mm-dd-yyyy') and to_date('"""
    SQL+=to_date.strftime("%m-%d-%Y")
    SQL+="""','mm-dd-yyyy')
          and EQPT_HARDWARE_ID = :tool
          ORDER BY LOCATION_TYPE ASC, ENTER_TS ASC                 
    """
    pars={"tool":toolId}
    wafer_df = psql.read_sql(SQL, db_conn,params=pars)

    log.info("No of Records in wafer history Data between %s & %s: %d, %s",from_date.strftime("%m-%d-%Y"),to_date.strftime("%m-%d-%Y"),wafer_df.shape[0],toolId)
    #print(SQL)

    wafer_df = wafer_df[wafer_df['SCRIBE'].isin(req_scribe_list)]
    wafer_df.drop_duplicates(keep='first',inplace=True)
    return wafer_df

def getPostEtchCFMDataFromCFM(tool_chamber, req_scribe_list, from_date, to_date):
    log.info('Getting CFM data from CFM DWR')
    
    global lot_list
    #print(lot_list)
    
    db_conn = cx_Oracle.connect('dcube_reader', 'dcub3_r3AdEr', cfm_dsn)
    cursor = db_conn.cursor()

    SQL="""
        with insp as (
            select 
            INSPECTION_ID 
            from 
            SYSTEMA_DWR2_ENG.V_SCAN_DATA_SHINY 
            where 
            LOTID in ("""
    SQL+=str(lot_list)[1:-1]
    SQL+=""") AND LAYER in ('PC-E-FS-042-POST','PC-E-BARE-042-SP3-HTO-POST','I1-CFMRIEHMTINBF-14LPP','I1-CFMSIONDF-14LPP','I3-CFMRIEHMTINBF-14LPP','I3-CFMRIEHMTINDF-14LPP',
        'I2-CFMRIEHMTINBF-14LPP','I2-CFMRIEHMTINDF-14LPP','M1-CFMPRIEDF-14LPP','M1-CFMPRIEBF-14LPP','M2-CFMPRIEDF-14LPP','M2-CFMPRIEBF-14LPP','M3-CFMPRIEDF-14LPP',
        'M3-CFMPRIEBF-14LPP','E-PARTICLE-POST-2.01','E-PARTICLE-POST.01','I1-CFMRIEHMTINBF-14LPP','I1-CFMSIONDF-14LPP','I3-CFMRIEHMTINBF-14LPP','I3-CFMRIEHMTINDF-14LPP', 'I2-CFMRIEHMTINBF-14LPP','I2-CFMRIEHMTINDF-14LPP','M1-CFMPRIEDF-14LPP','M1-CFMPRIEBF-14LPP','M2-CFMPRIEDF-14LPP','M2-CFMPRIEBF-14LPP','M3-CFMPRIEDF-14LPP', 'M3-CFMPRIEBF-14LPP', 'I1-CFMRIEHMTINBF-14HP','I1-CFMSIONDF-14HP','I3-CFMRIEHMTINBF-14HP','I3-CFMRIEHMTINDF-14HP', 'I2-CFMRIEHMTINBF-14HP','I2-CFMRIEHMTINDF-14HP','M1-CFMPRIEDF-14HP','M1-CFMPRIEBF-14HP','M2-CFMPRIEDF-14HP','M2-CFMPRIEBF-14HP','M3-CFMPRIEDF-14HP', 'M3-CFMPRIEBF-14HP','I1-CFMRIEHMTINBF-012LPPL00','I1-CFMSIONDF-012LPPL00','I3-CFMRIEHMTINBF-012LPPL00','I3-CFMRIEHMTINDF-012LPPL00', 'I2-CFMRIEHMTINBF-012LPPL00','I2-CFMRIEHMTINDF-012LPPL00','M1-CFMPRIEDF-012LPPL00','M1-CFMPRIEBF-012LPPL00','M2-CFMPRIEDF-012LPPL00','M2-CFMPRIEBF-012LPPL00','M3-CFMPRIEDF-012LPPL00', 'M3-CFMPRIEBF-14HP','I1-CFMRIEHMTINBF-14LPPDT',
        'I1-CFMSIONDF-14LPPDT','I3-CFMRIEHMTINBF-14LPPDT','I3-CFMRIEHMTINDF-14LPPDT', 'I2-CFMRIEHMTINBF-14LPPDT','I2-CFMRIEHMTINDF-14LPPDT','M1-CFMPRIEDF-14LPPDT','M1-CFMPRIEBF-14LPPDT','M2-CFMPRIEDF-14LPPDT','M2-CFMPRIEBF-14LPPDT','M3-CFMPRIEDF-14LPPDT', 'M3-CFMPRIEBF-14LPPDT','I1-CFMRIEHMTINBF-012FSOI-00','I1-CFMSIONDF-012FSOI-00','I3-CFMRIEHMTINBF-012FSOI-00','I3-CFMRIEHMTINDF-012FSOI-00', 'I2-CFMRIEHMTINBF-012FSOI-00','I2-CFMRIEHMTINDF-012FSOI-00','M1-CFMPRIEDF-012FSOI-00','M1-CFMPRIEBF-012FSOI-00','M2-CFMPRIEDF-012FSOI-00','M2-CFMPRIEBF-012FSOI-00','M3-CFMPRIEDF-012FSOI-00', 'M3-CFMPRIEBF-012FSOI-00','I1-CFMRIEHMTINBF-14LPE','I1-CFMSIONDF-14LPE','I3-CFMRIEHMTINBF-14LPE','I3-CFMRIEHMTINDF-14LPE', 'I2-CFMRIEHMTINBF-14LPE','I2-CFMRIEHMTINDF-14LPE','M1-CFMPRIEDF-14LPE','M1-CFMPRIEBF-14LPE','M2-CFMPRIEDF-14LPE','M2-CFMPRIEBF-14LPE','M3-CFMPRIEDF-14LPE', 'M3-CFMPRIEBF-14LPE')
            )
        select 
            cfmData.INSPECTION_ID,
            LOTID,
            INITFABLOT,
            WAFERSCRIBE as SCRIBE, 
            LAYER,
            PRODUCTGROUP,
            INSPECTIONTIME,
            TDD as TDD,
            TDC as TDC,
            NORM_DEF_DENSITY as NDD, 
            NORM_DEF_DENSITY_CLUSTERED as NDDc,
            NORM_DEF_DIE_PERC_WO_KR as NDDIEP,
            NORM_DEF_DENS_CLUST_WKR as NDDcWKr,
            NORM_DEF_DENS_CLUST_WB as NDDcWb,
            NORM_DEF_DENS_CLUST_WB_WKR as NDDcWbWKr,
            POI_NORM_DEF_COUNT as P_NDC,
            POI_NORM_DEF_COUNT_WKR as P_NDCwKR,
            POI_NORM_DEF_DENS as P_NDD,
            DOI_PNDDIE_WKR as PDDIEwKR,
            POI_NORM_DEF_DENS_WKR as P_NDDwKR,
            DEFECTCODE,
            DEFECTDESCRIPTION
        from 
            insp inner join  SYSTEMA_DWR2_ENG.V_SCAN_DATA_SHINY cfmData on insp.INSPECTION_ID =cfmData.INSPECTION_ID 
            left outer join SYSTEMA_DWR2_ENG.V_DEFECTCODE_DATA_SHINY meta on cfmData.INSPECTION_ID = meta.INSPECTION_ID"""
    
    req_layers = ['I1-CFMRIEHMTINBF-14LPP', 'I2-CFMRIEHMTINBF-14LPP', 'I2-CFMRIEHMTINDF-14LPP', 'I3-CFMRIEHMTINBF-14LPP',
                'I3-CFMRIEHMTINDF-14LPP', 'I2-CFMRIEHMTINDF-14HP']
    replace_layer = ['I1-RIEHMTIN-14LPP.01', 'I2-RIEHMTIN-14LPP.01', 'I2-RIEHMTIN-14LPP.01', 'I3-RIEHMTIN-14LPP.01',
                    'I3-RIEHMTIN-14LPP.01', 'I2-RIEHMTIN-14HP.01']
    
    post_etch_df = pd.DataFrame()    
    
    result_df = psql.read_sql(SQL, db_conn)
    log.info("No of Records in CFM Data between %s & %s: %d",from_date.strftime("%m-%d-%Y"),to_date.strftime("%m-%d-%Y"),result_df.shape[0])
    #result_df = pd.read_sql(SQL,conn)

    for i, layer in enumerate(req_layers):
        each_layer_df = result_df[result_df['LAYER']==layer]
        each_layer_df['LAYER'] = replace_layer[i]
        post_etch_df = post_etch_df.append(each_layer_df)
        post_etch_df.reset_index(drop=True, inplace=True)      

    #print(post_etch_df.head(10))

    post_etch_df = post_etch_df[post_etch_df['SCRIBE'].isin(req_scribe_list)]
    post_etch_df.reset_index(drop=True, inplace=True)

    return post_etch_df