# ETCH_prediction_V4.0.py

##prediction of the data split into model and prediction and splitting model into modelset and validationSet
import argparse
import pandas as pd
import numpy as np
import os,pickle, sys, json
import time
from eli5 import show_prediction
from Etch_data_aggr_for_prediction import get_aggr_data
import connexion
import pickle
from datetime import datetime
import openpyxl
from openpyxl import load_workbook
import itertools
from Write_Output import *
import logging
# import shap
import warnings
from Snowflake_IO import *
import json
warnings.filterwarnings("ignore", category=FutureWarning)

log = logging.getLogger(__name__)

pd.set_option('display.max_columns', 100)
pd.set_option('display.width', 50000)
pd.set_option('max_colwidth', 300)
pd.options.mode.chained_assignment = None


# calculating probability for best threshold
def predict_with_best_hyper(proba, threshold):
    predictions = []
    for i in proba:
        if i >= threshold:
            predictions.append(1)
        else:
            predictions.append(0)
    return predictions

def run_prediction(tool_chamber,date,ptms_file,fdc_dir,wafer_dir,inline_dir,post_etch_dir,model_dir,Model,prediction_output_file,prediction_contributing_factors_file,live_prediction):

    input_usage = "1"

    Model = 'MLP'
    prediction_data_end_date = date
    inline_space = inline_dir
    fdc_data = fdc_dir

    parent_dir = model_dir
    if parent_dir[-1] == '/':
        parent_dir = parent_dir + Model + "/" # Directory where model is saved
    else:
        parent_dir = parent_dir + "/" + Model + "/" # Directory where model is saved

    start = time.time()

    if parent_dir[-1] != '/':
        parent_dir = parent_dir + '/'
    with open(parent_dir + 'Model_Parameters.json') as f1:
        details_dict = json.load(f1)

    # path of standard scaler model file
    std_scaler_model = parent_dir+details_dict['Std_Model_File']

    # path of XGBoost model file
    model_path = parent_dir+details_dict['Model_File']

    # path of the parameters related to the model. eg: optimal threshold, hyperparameters of the model
    model_var = details_dict['Model_Var']

    opt_threshold = details_dict['Opt_Threshold']

    param = details_dict['Param']

    # data horizon of the data
    data_hzn = details_dict['data_hzn']

    # Name of the model
    mod = details_dict['mod']

    # name of the target column
    target_cols = details_dict['target_col']

    mod_var_median = details_dict['mod_var_median']
    ##################

    # Load the model for prediction
    # loading standardising model
    stdScaler = pickle.load(open(std_scaler_model, 'rb'))

    # loading model
    clf = pickle.load(open(model_path, 'rb'))

    # data horizon - data is for which month??
    data_hzn = data_hzn

    # model which is to be run
    mod = mod

    ###################################################################################################
    target_cols = target_cols

    mc_id = "ALL LamPN"
    try:
        pred = get_aggr_data(ptms_file, wafer_dir, inline_space, post_etch_dir, tool_chamber, fdc_data, prediction_data_end_date, input_usage,live_prediction)
    except Exception as e:

        print("Exception in run_prediction",e)
        return

    cols = model_var
    pred_data = pd.DataFrame(columns=cols)

    exists_cols = []
    for k in pred.columns:
        if k in cols:
            exists_cols.append(k)

    # fill na columns with median value of that particular column


    df1 = pd.DataFrame(columns = mod_var_median.keys())
    pred_cols = [k for k in pred.columns if k in mod_var_median.keys()]
    numData = pd.concat([df1,pred[pred_cols]])
    df_cols = list(set(mod_var_median.keys())-set(model_var))
    #numData.pop(df_cols[0])
    #numData.pop(df_cols[1])

    # standardizing the numerical vars
    num_df = stdScaler.transform(numData)

    # concatenating the numData with remaining categorical columns of original data
    num_df = pd.DataFrame(num_df, columns=numData.columns)

    # concatenating the numData with remaining categorical columns of original data
    nan_cols = num_df.columns[num_df.isna().any()].tolist()
    for c in nan_cols:
        num_df[c] = mod_var_median[c]

    # optimal threshold
    best_t = opt_threshold

    contribution_df = pd.DataFrame()

    time_ = str(datetime.now())
    time_ = time_.split('.')[0]

    

    # workbook_file_name = 'Prediction_Result_and_Contributing_factors_' + mod + '_v38_' + time_ + '.xlsx'
    # writer = pd.ExcelWriter(workbook_file_name, engine = 'openpyxl')

    get_contributing_factors = False
    if 'XGB' in mod or 'LR' in mod:
        print('Relavant variable only for',mod)
        clf,vec = clf
        no_missing = lambda feature_name, feature_value: not np.isnan(feature_value)
        x_pred = vec.transform(num_df[cols].to_dict('records'))
        y_train_prob = clf.predict_proba(x_pred)[:, 1]
        predictions_test = predict_with_best_hyper(y_train_prob, best_t)

        conf_df = pd.DataFrame(columns=['Prediction_date','predictions_train','prob'])
        conf_df['predictions_train'] = predictions_test
        conf_df['prob'] = y_train_prob
        conf_df.reset_index(drop=True, inplace=True)
        pred.reset_index(drop=True, inplace=True)
        conf_df['Prediction_date'] = pred['pred_end_time']
        print(mod,': Probability of Failure of Lam_PN for Machine', tool_chamber, ' on')
        conf_df=pd.concat([pred[['TOOL_CHAMBER','lam P/N','Serial Number','pred_start_time']],conf_df],axis=1)
        count_ = 0

        #Write prediction results to file        
        #conf_df.to_excel(writer, sheet_name = 'Prediction_Result', index = False)

        if os.path.exists(prediction_output_file): 
            conf_df.to_csv(prediction_output_file, mode='a',index =False)
        else:
            conf_df.to_csv(prediction_output_file, 'a',index =False)

        for row in num_df[cols].to_dict('records'):
            df = show_prediction(clf, row, vec=vec, show_feature_values=True,feature_filter = no_missing)
            df= pd.read_html(df.data)
            df = df[0]
            print(conf_df[conf_df.columns][count_:count_+1])
            # df.insert(0, 'DATE(USER_INPUT)', prediction_data_end_date)
            df.insert(0, 'pred_start_time', conf_df['pred_start_time'].iloc[count_])
            df.insert(1, 'TOOL_CHAMBER', conf_df['TOOL_CHAMBER'].iloc[count_])
            df.insert(2, 'LAM_PN', conf_df['lam P/N'].iloc[count_])
            df.insert(3, 'MODEL', mod)
            # col = ['pred_start_time', 'TOOL_CHAMBER', 'LAM_PN', 'MODEL', 'Feature', 'Contribution?', 'Value']
            
            #Write prediction contributions to file 
            #df.to_excel(writer, sheet_name = 'Contributing_factors_' + conf_df['lam P/N'].iloc[count_], index = False)

            if os.path.exists(prediction_output_file): 
                df.to_csv(prediction_contributing_factors_file, mode='a',index =False)
            else:
                df.to_csv(prediction_contributing_factors_file,index =False)

            count_ += 1
        get_contributing_factors = True
        log.info("Prediction complete")
        print("Succesfully obtained Contribution factors\nPrediction is completed")

    elif 'MLP':
        print("Running predictions")
        log.info("Running predictions with MLP model")
        clf,explainer = clf
        y_train_prob = clf.predict_proba(num_df[cols])[:, 1]
        predictions_test = predict_with_best_hyper(y_train_prob, best_t)

        conf_df = pd.DataFrame(columns=['Prediction_date','predictions_train','prob'])
        conf_df['predictions_train'] = predictions_test
        conf_df['prob'] = y_train_prob
        conf_df.reset_index(drop=True, inplace=True)
        pred.reset_index(drop=True, inplace=True)
        conf_df['Prediction_date'] = pred['pred_end_time']
        #print(mod,': Probability of Failure of Lam_PN for Machine', tool_chamber, ' on')
        conf_df=pd.concat([pred[['TOOL_CHAMBER','lam P/N','Serial Number','pred_start_time']],conf_df],axis=1)
        count_ = 0
        conf_df.columns= conf_df.columns.str.upper()

        #Add model name and version to prediction output
        conf_df['Model'] = details_dict['Model_File']

        #Write prediction results to file        
        #conf_df.to_excel(writer, sheet_name = 'Prediction_Result', index = False)

        write_results_to_file(conf_df,prediction_output_file) 
        writePredictionResultsToSnowflakeTable(conf_df)      

        for record in num_df[cols].values:
            df = pd.DataFrame()
            #print(conf_df[conf_df.columns][count_:count_+1])
            exp = explainer.explain_instance(record, clf.predict_proba, num_features=len(record))   
            rv_df = pd.DataFrame(list(exp.as_map().values())[0],columns=['Features','Contributions?'])
            all_cols = cols
            new_cols = [all_cols[int(k)] for k in rv_df['Features']]
            df['Features'] = [all_cols[int(k)] for k in rv_df['Features']]
            df['Values'] = num_df[new_cols].values[0]
            # df.insert(0, 'DATE(USER_INPUT)', prediction_data_end_date)
            df.insert(0, 'PRED_START_TIME', conf_df['PRED_START_TIME'].iloc[count_])
            df.insert(1, 'TOOL_CHAMBER', conf_df['TOOL_CHAMBER'].iloc[count_])
            df.insert(2, 'LAM_PN', conf_df['LAM P/N'].iloc[count_])
            df.insert(3, 'MODEL', mod)
            df = df.sort_values('Values',ascending=False)        

            #Write prediction contributions to file 
            #df.to_excel(writer, sheet_name = 'Contributing_factors_' + conf_df['lam P/N'].iloc[count_], index = False)
            write_results_to_file(df,prediction_contributing_factors_file)    
            writePredictionContributionsToSnowflakeTable(df)

            
            count_ += 1
        print("Succesfully obtained Contribution factors\nPrediction is completed")
        log.info("Prediction complete")

    # writer.save()
    # writer.close()

    end = time.time()
    print('Time Taken for Prediction:',round((end-start)/60, 2), " Min")    

    #contribution_df_filename = 'Features_contribution.csv'
    # contribution_df = contribution_df[['MODEL', 'TOOL_CHAMBER', 'LAM_PN', 'Contribution?', 'Feature', 'Value']]
    # contribution_df.to_csv(contribution_df_filename, index = False)

    #contribution_df_folderpath = os. getcwd()
    #contribution_df_file_path = contribution_df_folderpath + '/' + contribution_df_filename
    #print("Features contributing for prediction result is stored at: ", contribution_df_filename)