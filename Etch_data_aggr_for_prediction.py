import pandas as pd
import numpy as np
from tqdm import tqdm
import timeit
from datetime import date, datetime, time, timedelta
import os
import time
import re
from re import search
import math
import sys
import glob, os
from sklearn.linear_model import LinearRegression
import xlrd
import warnings
from Data_Retrieval import *
from Data_Retrieval_Sapphire import *
import logging
warnings.filterwarnings("ignore", category=FutureWarning)
pd.options.mode.chained_assignment = None

log = logging.getLogger(__name__)

def get_nInstall(s_n):
    trailing = []
    if 'EXT' in s_n or 'ext' in s_n:
        regex = re.compile('[a-zA-Z]*[0-9]*[a-zA-Z]*[0-9]*([a-zA-Z]*)$') # check for ETX2922PM2190110XX or etx2922pm2190110XX
        trailing = regex.findall(s_n)
        if len(trailing) > 0:
            new_s_n = re.sub(trailing, '', s_n)[0]
            
    elif '-' in s_n:
        splits = s_n.split('-')
        if len(splits) == 4:
            regex = re.compile('[0-9]*-[0-9]*-[0-9]*-[0-9]*([a-zA-Z]*)$') # check for 1015927-14-08-6249XXX
            trailing = regex.findall(s_n)
            if len(trailing) > 0:
                trailing = regex.findall(s_n)[0]
                new_s_n = re.sub(trailing, '', s_n)
            
            if len(trailing) == 0:
                splits = s_n.split('-')
                if len(splits) == 4:
                    regex = re.compile('[0-9]*-[0-9]*-[0-9]*-[0-9]*[a-zA-Z]*[0-9]*([a-zA-Z]*)$') # check for 1015927-14-08-6249XX98XX
                    trailing = regex.findall(s_n)
                    if len(trailing) > 0:
                        trailing = regex.findall(s_n)[0]
                        new_s_n = re.sub(trailing, '', s_n)
    elif s_n.isalnum():
        regex = re.compile('[0-9]*([a-zA-Z]*)$') # check for 09876XX
        trailing = regex.findall(s_n)[0]
        if len(trailing) > 0:
            trailing = regex.findall(s_n)[0]
            new_s_n = re.sub(trailing, '', s_n)
    elif s_n.isalnum():
        regex = re.compile('[0-9]*[a-zA-Z]*[0-9]*([a-zA-Z]*)$') # check for 09876XX
        trailing = regex.findall(s_n)
        if len(trailing) > 0:
            trailing = regex.findall(s_n)[0]
            new_s_n = re.sub(trailing, '', s_n)
    return len(trailing)


def window_stack(a, stepsize, width):
    n = a.shape[0]
    return np.hstack(a[i:1+n+i-width:stepsize] for i in range(0,width))



def Pred_DataPreparation(mod_data):
    final_df1 = pd.DataFrame()
    OW_LA_PW=[7,3,1]
    # mod_data=pd.read_csv(file_name)
    mod_data_columns=pd.DataFrame(mod_data.columns)
    ##look forward into future 
    target_slide=OW_LA_PW[2]
    ## the size of data defined by abhishek data
    data_step_size=OW_LA_PW[1]
    ## window size defined to roll over 
    window_size=OW_LA_PW[0]
    ## the window size  
    slide_over=int(window_size)
    #target sliding window size 
    target_label="Removal"
    #target sliding window size 
    target_index=int(window_size+target_slide+data_step_size)
    mod_data["Part_Id"]=mod_data["TOOL_CHAMBER"]+"_"+mod_data["lam P/N"]+"_"+mod_data["Serial Number"]
    #Remove_LT_Window=1 #### Removing lessthan windowed data give 1 else 0
    unlam_sn=mod_data['Part_Id'].unique()

    for ls in range(len(unlam_sn)):
        df=mod_data[mod_data['Part_Id']==unlam_sn[ls]]
        if df.shape[0]>=window_size:
            un_sn=df['Serial Number'].unique()
            un_tool=df['TOOL_CHAMBER'].unique()
            un_lam=df["lam P/N"].unique()
            un_part=unlam_sn[ls]
            dff=df.reset_index()
            df['START_TIME'] =  pd.to_datetime(df['START_TIME'])
            df = df.sort_values(by='START_TIME')
            df.reset_index(drop=True,inplace=True)
            #target=df[target_label]
            #target = target.iloc[target_index-1:,:].reset_index(drop=True)
            #df.drop(target_label,axis=1,inplace=True)
            for i in list(df.columns):
                if 'TIME' in i:
                    df.drop(i,axis=1,inplace=True)
            df.drop(['TOOL_CHAMBER','lam P/N','Serial Number','Part_Id'],axis=1,inplace=True)
            columns_list=df.columns

            concatinated_list_names=[]
            all_names_list = [[] for i in range(0,slide_over)]

            for i in all_names_list:
                #print(all_names_list.index(i))
                for j in columns_list:
                    i.append(j+"_"+str(all_names_list.index(i)))

            for i in all_names_list:
                concatinated_list_names=concatinated_list_names+i
            ## creating the sliding data_frame
            windowed=window_stack(np.array(df),1,slide_over)
            final_df=pd.DataFrame(windowed,columns=concatinated_list_names)
            shape=list(final_df.shape)
            df_len=len(final_df)
            # caliculate start end time 
            tool=[]
            lampn=[]
            serialno=[]
            Part_Id=[]
            START_TIME=[]
            END_TIME=[]
            PRED_START_TIME=[]
            PRED_END_TIME=[]
            for i in range(0,len(final_df)):
                st=dff['START_TIME'][i]
                et=dff['END_TIME'][i+(slide_over-1)]
                START_TIME.append(st)
                END_TIME.append(et)
                pred_start = pd.to_datetime(et)+ timedelta(days=target_slide+data_step_size)
                PRED_START_TIME.append(str(pred_start))
                pred_end = pred_start + timedelta(days=target_slide)
                PRED_END_TIME.append(str(pred_end))
                tool.append(un_tool[0])
                lampn.append(un_lam[0])
                serialno.append(un_sn[0])
                Part_Id.append(un_part)
            final_df.insert(loc=0, column='TOOL_CHAMBER', value=tool)
            final_df.insert(loc=1, column='lam P/N', value=lampn)
            final_df.insert(loc=2, column='Serial Number', value=serialno)
            final_df.insert(loc=3, column='start_time', value=START_TIME)
            final_df.insert(loc=4, column='end_time', value=END_TIME)
            final_df.insert(loc=5, column='pred_start_time', value=PRED_START_TIME)
            final_df.insert(loc=6, column='pred_end_time', value=PRED_END_TIME)
            final_df.insert(loc=7, column='Part_Id', value=Part_Id)
            final_df["TIME"]="00:00:00"
            final_df["start_time"]=final_df["start_time"].astype(str)+" "+final_df["TIME"]
            final_df["end_time"]=final_df["end_time"].astype(str)+" "+final_df["TIME"]
            final_df.drop("TIME", axis = 1,inplace = True) 
            if ls==0:
                final_df1=final_df
            else:
                final_df1=pd.concat([final_df1,final_df])
        else:
            print("Not Enough data for prediction")
            raise Exception("Not Enough data for prediction")
            #sys.exit()
            

    if final_df1.shape[0]>0:    
        Prior_install_col = [col for col in final_df1 if col.startswith('Prior_nInstall')]
        final_df1["nInstall_0"]=final_df1["Prior_nInstall_0"]
        final_df1.drop(Prior_install_col, axis = 1,inplace = True)
        final_df1.rename(columns={"nInstall_0": "Prior_nInstall_0"},inplace = True) 
        print("Model Data Preparation SUCCESS!")
        log.info("Model data prep complete, Starting prediction")
        print("\nStarting Prediction")
    else:
        print("Not Enough data for prediction for Lam_PN's\n TERMINATING PROGRAM!!!")
        raise Exception("Not Enough data for prediction for Lam_PN's\n TERMINATING PROGRAM!!!")
        #sys.exit()
    return final_df1 

def compute_months(first_date, second_date):
    year1, month1, year2, month2 = map(
        int, 
        (first_date[:4], first_date[5:7], second_date[:4], second_date[5:7])
    )

    return [
        '{:0>4}-{:0>2}'.format(year, month)
        for year in range(year1, year2 + 1)
        for month in range(month1 if year == year1 else 1, month2 + 1 if year == year2 else 13)
    ]


month_dict = {'01':'jan', '02':'feb','03':'mar', '04':'apr','05':'may', '06':'jun','07':'jul', '08':'aug','09':'sep', '10':'oct','11':'nov', '12':'dec'}


def get_wafer_data(wafer_path, tool_chamber, req_scribe_list, from_date, to_date):
    req_tool = tool_chamber.split('-')[0]
    # if wafer_path[-1] != '/':
    #     wafer_path = wafer_path + '/'
    # wafer_files = os.listdir(wafer_path)
    wafer_cols = ['SCRIBE', 'STEP', 'TOOL', 'DATE_STEP_STARTED', 'DATE_STEP_FINISHED']
    wafer_df = pd.DataFrame()

    try:
        #wafer_df = getWaferHistoryDataFromAthena(tool_chamber,req_scribe_list,from_date,to_date)
        wafer_df = getWaferHistoryDataFromSapphire(tool_chamber,req_scribe_list,from_date,to_date)
    except Exception as e:
            print("Exception in get_wafer_data",e)
            raise Exception("get_wafer_data query failed")
            

    # for file_ in wafer_files:
    #     if '.csv' in file_:
    #         temp_ = pd.read_csv(wafer_path + file_)
    #         temp_ = temp_[temp_['TOOL'] == req_tool]
    #         temp_ = temp_[wafer_cols]
    #         temp_ = temp_[temp_['SCRIBE'].isin(req_scribe_list)]
    #         temp_.drop_duplicates(keep='first', inplace=True)
    #         wafer_df = wafer_df.append(temp_)
    #     else:
    #         with open(wafer_path + file_,encoding='UTF-16') as f:
    #             temp_ = pd.read_csv(f)
    #             temp_ = temp_[temp_['TOOL'] == req_tool]
    #             temp_ = temp_[wafer_cols]
    #             temp_ = temp_[temp_['SCRIBE'].isin(req_scribe_list)]
    #             temp_.drop_duplicates(keep='first', inplace=True)
    #             wafer_df = wafer_df.append(temp_)
                
    wafer_df["SS"] = wafer_df['SCRIBE']  + '_' + wafer_df['STEP']
    wafer_df.reset_index(drop=True, inplace=True)
    print("READ WAFER_HISTORY DATA")

    return wafer_df



def get_inline_space_data(wafer_path, inline_space_path, tool_chamber, req_scribe_list, from_date, to_date):
    req_tool = tool_chamber.split('-')[0]
    # if inline_space_path[-1] != '/':
    #     inline_space_path = inline_space_path + '/'
    re_IS_cols = ['TOOL', 'CHAMBER', 'SCRIBE', 'PROCESS_STEP', 'RAW_VALUE','LSL', 'USL']

    # inline_space_files = os.listdir(inline_space_path)

    inline_space_df = pd.DataFrame()
    
    # for file_ in inline_space_files:
    #     temp_ = pd.read_csv(inline_space_path + file_, usecols = re_IS_cols)
    #     temp_ = temp_[temp_['TOOL'] == req_tool]
    #     inline_space_df = inline_space_df.append(temp_)
    #inline_space_df = getInlineSpaceDataFromAthena(tool_chamber,req_scribe_list,from_date,to_date)
    inline_space_df = getInlineSpaceDataFromGEMD(tool_chamber,req_scribe_list,from_date,to_date)
    

    inline_space_df['TARGET'] = 0
    inline_space_df.rename(columns={'PROCESS_STEP': 'STEP'}, inplace=True)
    inline_space_df.reset_index(drop=True, inplace=True)
    high = inline_space_df.index[inline_space_df['RAW_VALUE'] > inline_space_df['USL']]
    low = inline_space_df.index[inline_space_df['RAW_VALUE'] < inline_space_df['LSL']]
    all_ = list(low) + list(high)
    inline_space_df.at[all_, 'TARGET'] = 1
    inline_scribe_list = list(inline_space_df.SCRIBE.unique())
    inline_space_df = inline_space_df[inline_space_df['SCRIBE'].isin(req_scribe_list)]
    inline_space_df["SS"] = inline_space_df['SCRIBE']  + '_' + inline_space_df['STEP']
    inline_space_df.reset_index(drop=True, inplace=True)
    inline_space_df.rename(columns={'PARAMETER_NAME': 'PARAM'}, inplace=True)
    inline_space_df.reset_index(drop=True, inplace=True)
    print("READ INLINE SPACE DATA")
    final_inline_space_df = pd.DataFrame()
    if inline_space_df.shape[0] > 0:
        wafer_df = get_wafer_data(wafer_path, tool_chamber, req_scribe_list, from_date, to_date)
        # MERGE INLINE_SPACE & WAFER DATA TO EXTRACT "DATE_STEP_FINISHED"
        inline_space_time_df = pd.merge(inline_space_df,
                    wafer_df[['DATE_STEP_FINISHED', 'SS']],
                    on='SS', 
                    how='left')
        inline_space_time_df = inline_space_time_df[['SCRIBE', 'STEP', 'DATE_STEP_FINISHED', 'TARGET']]
        inline_space_time_df.reset_index(drop=True, inplace=True)
        grouped_df = inline_space_time_df.groupby(['SCRIBE', 'STEP', 'DATE_STEP_FINISHED'])
        final_inline_space_df = pd.DataFrame()
        for (sc,st,da),df_ in grouped_df:
            shape = df_.shape[0]
            ntarget = df_.TARGET.sum()
            percent = round(ntarget*100/shape, 2)
            final_inline_space_df = final_inline_space_df.append({'SCRIBE':sc, 'STEP':st, 'DATE_STEP_FINISHED':da, 'TOTAL_nDATA':shape, 'TOTAL_nOUTLIER':ntarget, 'TOTAL_%OUTLIER':percent}, ignore_index=True)
        final_inline_space_df['DATE_STEP_FINISHED'] = pd.to_datetime(final_inline_space_df['DATE_STEP_FINISHED'])
        final_inline_space_df = final_inline_space_df[(final_inline_space_df['DATE_STEP_FINISHED'] >= from_date) & (final_inline_space_df['DATE_STEP_FINISHED']<= to_date)]
        final_inline_space_df.reset_index(drop=True, inplace=True)
        final_inline_space_df['SCRIBE_STEP'] = final_inline_space_df['SCRIBE'] + "_" + final_inline_space_df['STEP']
    else:
        print("Inline_Space data for matching Scribe's is empty")
    
    return final_inline_space_df

def get_post_etch_data(post_etch_path, tool_chamber, req_scribe_list, from_date, to_date):
    req_tool = tool_chamber.split('-')[0]
    # if post_etch_path[-1] != '/':
    #     post_etch_path = post_etch_path + '/'
    # post_etch_files = os.listdir(post_etch_path)

    req_post_etch_cols = ['SCRIBE', 'LAYER', 'INSPECTIONTIME', 'NDD', 'DEFECTDESCRIPTION']
    DEFECTDESCRIPTION = ['Surface Particle', 'Missing Pattern', 'Single Line Open', 'Single Line Open Type 2']
    req_layers = ['I1-CFMRIEHMTINBF-14LPP', 'I2-CFMRIEHMTINBF-14LPP', 'I2-CFMRIEHMTINDF-14LPP', 'I3-CFMRIEHMTINBF-14LPP',
                'I3-CFMRIEHMTINDF-14LPP', 'I2-CFMRIEHMTINDF-14HP']
    replace_layer = ['I1-RIEHMTIN-14LPP.01', 'I2-RIEHMTIN-14LPP.01', 'I2-RIEHMTIN-14LPP.01', 'I3-RIEHMTIN-14LPP.01',
                    'I3-RIEHMTIN-14LPP.01', 'I2-RIEHMTIN-14HP.01']

    # post_etch_df = pd.DataFrame()
    # for file_ in post_etch_files:
    #     temp_ = pd.read_csv(post_etch_path + file_, usecols = req_post_etch_cols)
    #     temp_ = temp_[temp_['LAYER'].isin(req_layers)]
    #     temp_ = temp_[temp_['DEFECTDESCRIPTION'].isin(DEFECTDESCRIPTION)]
    #     for i, layer in enumerate(req_layers):
    #         each_layer_df = temp_[temp_['LAYER']==layer]
    #         each_layer_df['LAYER'] = replace_layer[i]
    #         post_etch_df = post_etch_df.append(each_layer_df)
    #         post_etch_df.reset_index(drop=True, inplace=True)
    # post_etch_df = post_etch_df[post_etch_df['SCRIBE'].isin(req_scribe_list)]
    # post_etch_df.reset_index(drop=True, inplace=True)
    #print(post_etch_df.head(10))
    
    #post_etch_df = getPostEtchCFMDataFromAthena(tool_chamber, req_scribe_list, from_date, to_date)    
    post_etch_df = getPostEtchCFMDataFromCFM(tool_chamber, req_scribe_list, from_date, to_date)  

    if post_etch_df.shape[0] > 0:
        
        post_etch_df.rename(columns={'LAYER': 'STEP'}, inplace=True)
        
        post_etch_df['INSPECTIONTIME'] = pd.to_datetime(post_etch_df['INSPECTIONTIME'])
        #post_etch_df = post_etch_df[(post_etch_df['INSPECTIONTIME'] >= from_date) & (post_etch_df['INSPECTIONTIME']<= to_date)]
        #req_post_etch_cols = ['SCRIBE', 'STEP', 'INSPECTIONTIME', 'NDD']
        #post_etch_df = post_etch_df[req_post_etch_cols]
        post_etch_df.reset_index(drop=True, inplace=True)
        post_etch_df['SCRIBE_STEP'] = post_etch_df['SCRIBE'] + "_" + post_etch_df['STEP']
    else:
        print("Post_Etch data for matching Scribe's is empty")
    
    #print(post_etch_df.head(10))
    print("READ POST_ETCH DATA")
    return post_etch_df


def get_fdc_data(fdc_files_path, fdc_files, tool_id, date_list, module_id, data_start_time, data_end_time):
    fdc_df = pd.DataFrame()
    
    # read_fdc_files_list = []
    
    # part_start_time = datetime.now()

    # for filename_ in fdc_files:
    #     if tool_id in filename_:
    #         for date_ in date_list:
    #             year = str(date_).split('-')[0]
    #             month = str(date_).split('-')[1]
    #             month = month_dict[month]
    #             check_date = str(year) in filename_ and month in filename_.lower()
    #             if check_date :
    #                 read_fdc_files_list.append(filename_)
    #                 temp_fdc = pd.read_csv(fdc_files_path + filename_, low_memory=False)
    #                 read_fdc_files = True
    #                 temp_fdc = temp_fdc.loc[temp_fdc['TOOL_CHAMBER'] == module_id]
    #                 temp_fdc.reset_index(drop=True, inplace=True)
    #                 if temp_fdc.shape[0]>0:
    #                     for date_col in temp_fdc.columns:
    #                         if "TIME" in date_col:
    #                             remove_index_list = []
    #                             for ind, element in enumerate(temp_fdc[date_col]):
    #                                 if 'TIME' in element:
    #                                     remove_index_list.append(ind)
    #                                 else:
    #                                     date = element.split(' ')[0]
    #                                     year = date.split('-')[0]
    #                                     if int(year) < 2019:
    #                                         remove_index_list.append(ind)
    #                             if len(remove_index_list) > 0:
    #                                 temp_fdc.drop(temp_fdc.index[remove_index_list], inplace=True)
    #                     temp_fdc.reset_index(drop=True, inplace=True)
    #                 temp_fdc['START_TIME'] =  pd.to_datetime(temp_fdc['START_TIME'])
    #                 temp_fdc['END_TIME'] =  pd.to_datetime(temp_fdc['END_TIME'])
    #                 temp_fdc = temp_fdc.loc[(temp_fdc['START_TIME']>= data_start_time) & (temp_fdc['END_TIME']<= data_end_time)]
    #                 temp_fdc.reset_index(drop=True, inplace=True)
    #                 fdc_df = fdc_df.append(temp_fdc)    

    # part_end_time = datetime.now()
    # time_per_part = part_end_time- part_start_time
    # print("FDC_KN Files Read: ", read_fdc_files_list, "Time taken to read FDC_KN file : ", time_per_part)
    try:
        #fdc_df = getFDCKNDataFromAthena(tool_id, date_list, module_id, data_start_time, data_end_time)
        fdc_df = getFDCKNDataFromSapphire(tool_id, date_list, module_id, data_start_time, data_end_time)
    except Exception as e:
            print("Exception in get_fdc_data",e)
            raise Exception("getFDCKNDataFromAthena failed") 

    return fdc_df

def get_aggr_data(ptms_file_name, wafer_path, inline_space_path, post_etch_path, tool_chamber, fdc_files_path, to_date, input_usage,live_prediction):
    agg_start_time = datetime.now()
    check_tool = []
    tool_chamber = tool_chamber.upper()
    to_date = datetime.strptime(to_date, '%d-%m-%Y')
    from_date = to_date - pd.Timedelta(days=7)
    log.info("INPUT Prediction_Data_Start_Date :%s,Prediction_Data_End_Date :%s", from_date, to_date)
    if '-' in tool_chamber:
        tool = tool_chamber.split('-')[0]
        chamber = tool_chamber.split('-')[1]
        check_tool = [tool]
    else:
        print("Tool-Chamber(ETX2930X-PMX) both are required")
        log.error("Tool-Chamber(ETX2930X-PMX) both are required")
        raise Exception("Tool-Chamber(ETX2930X-PMX) both are required")
        #sys.exit()
    req_tool = ['ETX29300', 'ETX29301', 'ETX29302', 'ETX29303', 'ETX29304','ETX29305']

    if check_tool[0] not in req_tool:
        print("Entered TOOL is not in Required TOOL's.\nKINDLY USE ONE OF THE FOLLOWING TOOLS:\n", req_tool)
        log.error("Entered TOOL is not in Required tools")
        raise Exception("Entered TOOL is not in Required TOOL's.\nKINDLY USE ONE OF THE FOLLOWING TOOLS:\n", req_tool)
        #sys.exit()
    
    # req_lampn = ['719-003481-858', '715-045710-821', '715-042721-866', '715-027638-822', '714-045743-809', '000-300101-956']
    #  # For Reading All sheets from Customer's PTMS Excel Data
    # ptms_fields = ['Tool ID', 'Module ID', 'Lam P/N', 'Serial Number', 'Install Date', 'Removal Date', 'Usage At Install', 'Usage At Removal', 'Usage During This Install']
    # xls = xlrd.open_workbook(ptms_file_name, on_demand=True) # READ EXCEL FILE TO GET SHEET NAMES
    # sheet_names = xls.sheet_names() # Now you can list all sheets in the file
    # ptms_df = pd.DataFrame()
    # for sheet in sheet_names:
    #     temp_df = pd.read_excel(ptms_file_name, sheet_name=sheet, skiprows=3, usecols=ptms_fields)
    #     ptms_df = ptms_df.append(temp_df)
    # ptms_df.drop_duplicates(keep='first', inplace=True)
    # ptms_df['Install Date'] = pd.to_datetime(ptms_df['Install Date'], format='%m/%d/%Y %I:%M:%S %p')
    # ptms_df['Removal Date'] = pd.to_datetime(ptms_df['Removal Date'], format='%m/%d/%Y %I:%M:%S %p')
    # ptms_df= ptms_df[ptms_df['Lam P/N'].isin(req_lampn)]
    # # temp_df = temp_df[(temp_df['Removal Date'].isna()) & (temp_df['Usage At Removal'].isna())] ## FOR LIVE DATA PREDICTION
    # ptms_df = ptms_df[(ptms_df['Install Date'] <=  from_date) & (ptms_df['Removal Date'] >=  to_date) & (ptms_df['Module ID'] == tool_chamber)] ## FOR HISTORICAL DATA PREDICTION
    # ptms_df = ptms_df[ptms_fields]
    # ptms_df.reset_index(drop=True, inplace=True)
    # if ptms_df.shape[0] > 0:
    #     print("Read PTMS File, No of Parts For Prediction: ", ptms_df.shape[0])
    # else:
    #     print("PTMS Data doesn't contain required LAM-PN's or Machine;s/Tool's")
    #     sys.exit()

    # ## If Prior_Usage is Taken from the User then change 'prior_usage_UserInput' to 'True
    # if str(input_usage) == '1':
    #     prior_usage_UserInput = False
    # else:
    #     prior_usage_UserInput = True

    # if prior_usage_UserInput:
    #     # ptms_df = ptms_df[['Tool ID', 'Module ID', 'Lam P/N', 'Serial Number', 'Usage At Install', 'Install Date']]
    #     save_ptms_df = ptms_df[['Serial Number']].copy()
    #     save_ptms_df.insert(len(save_ptms_df.columns), "Prior_Usage", "")
    #     save_ptms_df.to_csv("Prior_Usage.csv", index = False)
    #     print("\nKindly fill 'Prior_Usage' column in 'Prior_Usage.csv' file, which is in present working directory and save the file")
    #     continue_ = input("\tIf the CSV file is updated press 1 to continue or 0 to exit:\t")
    #     if not int(continue_):
    #         print("Terminating Program")
    #         sys.exit()
    #     save_ptms_df = pd.read_csv("Prior_Usage.csv")
    #     ptms_df = pd.merge(ptms_df, save_ptms_df, on='Serial Number', how='left')
    #     if ptms_df.shape[0] > 0:
    #         ptms_df['Install Date'] = pd.to_datetime(ptms_df['Install Date'])
    #         print("\tUpdated 'Prior_Usage.csv' File")
    #         print("No of Parts For Prediction: ", ptms_df.shape[0])
    #     else:
    #         print("PTMS Data doesn't contain required LAM-PN's or Machine's/Tool's")
    #         sys.exit()
    # ## Automating Prior_Usage calculation ONLY FOR HISTORICAL DATA PREDICTION
    # else:
    #     ptms_df['Prior_Usage'] = 0
    #     for ind, row in ptms_df.iterrows():
    #         install_date = row['Install Date']
    #         removal_date = row['Removal Date']
    #         total_days = (removal_date-install_date).days
    #         usage_at_install = row['Usage At Install']
    #         usage_at_removal = row['Usage At Removal']
    #         usage = row['Usage During This Install']
    #         no_of_days_prior_observation = (from_date - install_date).days
    #         prior_usage = round(usage*no_of_days_prior_observation/total_days, 1)
    #         ptms_df['Prior_Usage'].iloc[ind] = prior_usage + usage_at_install
    # # print(ptms_df[['Lam P/N', 'Serial Number', 'Install Date', 'Usage At Install', 'Prior_Usage']])
    # print(ptms_df.head(10))

    
    ptms_df = getPTMSDataFromFile(ptms_file_name,from_date,to_date,tool_chamber,live_prediction)
    

    
    # PTMS FILE UPDATED    
    # if fdc_files_path[-1] != "/":
    #     fdc_files_path = fdc_files_path + "/"
    # available_fdc_files = os.listdir(fdc_files_path)
    fdc_files=[]
    # fdc_files = [file_ for file_ in available_fdc_files if 'ETX' in file_]
    fdc_unique_cols = ['MFG_AREA_NAME', 'TECHNOLOGY', 'PRODUCT_GROUP_ID', 'PRODUCT_ID', 'CTRL_JOB', 'STEP', 'RECIPE_MACHINE_ID', 'RECIPE_LOGICAL_ID', 'FDC_CONTEXT_ID', 'RECIPE_NAME', 'APPLICATION_NAME', 'APPLICATION_VERSION', 'MODEL_STATUS', 'STEP_ID', 'EQPT_HARDWARE_CLASS', 'EQPT_HARDWARE_TYPE']
    fdc_concat_cols = ['LOT_ID', 'SCRIBE', 'WAFER_NUMBER']
    fdc_concat_col = '_'.join(fdc_concat_cols)
    tool_id = check_tool[0]
    module_id = ptms_df['Module ID'].iloc[0]
    module_id = module_id.replace("-", "_")
    start = from_date
    end = to_date
    data_start_time = from_date
    data_end_time = to_date
    date_list = compute_months(str(data_start_time), str(data_end_time))
    # Read Required FDC File/Files
    req_files_ = []
    master_fdc_df = pd.DataFrame()
    master_fdc_df = get_fdc_data(fdc_files_path, fdc_files, tool_id, date_list, module_id, data_start_time, data_end_time)
    if master_fdc_df.shape[0] == 0:
        print("FDC Data is empty")
        print("Terminating Program")
        log.error("FDC data is empty, termintating program")
        raise Exception("FDC Data is empty\n Terminating Program")
        #sys.exit()

    no_of_days = 7
    no_of_hrs = 24
    no_of_hrs_from_removal = no_of_days * no_of_hrs
    count_parts_created = 0
    mod_data_created = False
    fdc_data_present = False

    for index, row in tqdm(ptms_df.iterrows()):
        fdc_df = pd.DataFrame()
        tool_id = row['Tool ID']
        module_id = str(row['Module ID'])
        lam_pn = row['Lam P/N']
        serial_no = row['Serial Number']
        p_install = get_nInstall(serial_no)
        p_usage = row['Prior_Usage']
        install_date = row['Install Date']
        if install_date >= from_date:
            data_start_time = install_date
        fdc_df = master_fdc_df.loc[(master_fdc_df['START_TIME']>= data_start_time) & (master_fdc_df['END_TIME']<= data_end_time)]
        fdc_df.reset_index(drop=True, inplace=True)
        if fdc_df.shape[0]>0:
            fdc_data_present = True
            count_parts_created += 1
            fdc_df.reset_index(drop=True, inplace=True)
            start_hr = fdc_df['START_TIME'].min()
            start_hr = start_hr.replace(minute=0, hour=0, second=0)
            time_list = [start_hr]
            i = start_hr
            while i < end:
                i = i + pd.Timedelta(days=1)
                time_list.append(i)
            hr_start = []
            hr_end = []
            for i in range(len(time_list)-1):
                hr_start.append(time_list[i])
                hr_end.append(time_list[i+1])
            data = pd.DataFrame()
            time_list.reverse()
            time_horizon = no_of_days
            time_slot = 60
            day_slot = 1
            if day_slot == 1:
                hrs = 24
            final_prior_usage = []
            day_count = 0
            for i in range(len(time_list)-1):
                masked_dataframe = fdc_df.loc[(fdc_df['START_TIME'] >= time_list[i+1]) & (fdc_df['END_TIME'] < time_list[i])]
                if masked_dataframe.shape[0]>1:
                    day_count += 1
                    data = data.append(masked_dataframe)
                    p_usage = p_usage + 24
                    final_prior_usage.append(p_usage)
                else:
                    final_prior_usage.append(p_usage)
                if day_count == 7:
                    break
            if day_count < 7:
                print("Not Enough data for prediction")
                log.error("Can't find at least 7 days of data, terminating")
                raise Exception("Not Enough data for prediction")
                #sys.exit()

            priorusage = p_usage
            priorinstall = p_install
            data = data[data['SENSOR_NAME'].notna()] ## Merged_df
            data['FDC_RAW'] = pd.to_numeric(data['FDC_RAW'])
            #create the diff and show in seconds 
            mean_cols = []
            sensor_mean=data['SENSOR_NAME'].unique().tolist()
            for i in range(len(sensor_mean)):
                fullstring = sensor_mean[i]
                sub_Mean = "_Mean"
                if search(sub_Mean,fullstring):
                    mean_cols.append(fullstring)
            df = data.copy() #important as "df" is referenced henceforth
            df = df.sort_values(by='START_TIME') # sort the dataframe
            df.reset_index(drop=True,inplace=True)
            df['START_TIME'][0]
            st_time=df['START_TIME'][0].replace(hour=0, minute=0,second=0)
            time_ = [st_time]  #intially mentioned the start date
            e = data_end_time
            i = st_time  #start_time
            while i < e:
                # i = i + pd.Timedelta(minutes = time_slot)
                i = i + pd.Timedelta(days = day_slot)
                time_.append(i) 
                # function to mask into dataframe
            def mask_date(start,end,dataframe):
                """start: enter the start time
                    end:enter the end time"""
                mask = (dataframe['START_TIME'] >= start) & (dataframe['END_TIME'] < end)
                dataframe = dataframe.loc[mask]
                dataframe.reset_index(inplace = True)
                return dataframe
            #function for max of min values

            def cal_max(data,var,weight_var):
                result  = data[var].max()
                return result 

            #function for min of min values
            def cal_min(data,var,weight_var):
                result = data[var].min()
                return result
            #function for Mean

            def cal_wavg(data, var, weight_var):
                d = data[var]
                w = data[weight_var]

                try:
                    return (d * w).sum() / w.sum()
                except ZeroDivisionError:
                    return np.nan
            #function for Stdev
            def cal_weighted_std(data,var,weight_var):
                values = data[var]
                try:

                    weights = data[weight_var]
                    average = np.average(values, weights=weights)
                    # Fast and numerically precise:
                    variance = np.average((values-average)**2, weights=weights)
                    return (math.sqrt(variance))

                except ZeroDivisionError:
                    return np.nan   
            #function for Slope

            def cal_slope(data,var2,var1):
                regressor = LinearRegression()
                if  data.shape[0]>0:
                    x = data[var1].values.reshape(-1,1)
                    y = data[var2].values.reshape(-1,1)
                    regressor.fit(x,y)
                    coef=regressor.coef_
                    coef=coef[0][0]
                if  data.shape[0]==0:
                    coef=np.nan
                return coef


            #################  aggregation for 60 min  #################

            output = pd.DataFrame()
            START_TIME = []
            END_TIME = []
            START_TIME = []
            END_TIME = []
            nData=[]
            #################  Getting Start and end time  ################# 


            #################  Adding count of unique items in columns  #################
            n_unique_cols = []
            for col in fdc_unique_cols:
                n_unique_cols.append('n'+col)
            fdc_unique_cols_values = []
            concat_col = []
            for i in range(len(time_)-1):
                START_TIME.append(time_[i])
                END_TIME.append(time_[i+1])
                j = len(mask_date(start=time_[i],end=time_[i+1],dataframe=df))
                nData.append(j)
                #################  Adding count of unique items in columns  #################
                fdc_unique_cols_values_temp = []
                for col in fdc_unique_cols:
                    temp_mask = mask_date(start=time_[i],end=time_[i+1],dataframe=df)
                    fdc_unique_cols_values_temp.append(len(temp_mask[col].unique()))
                fdc_unique_cols_values.append(fdc_unique_cols_values_temp)
                temp_mask = mask_date(start=time_[i],end=time_[i+1],dataframe=df)
                temp_mask['WAFER_NUMBER'] = temp_mask['WAFER_NUMBER'].astype(str)
                temp_mask[fdc_concat_col] = temp_mask['LOT_ID'] + temp_mask['SCRIBE'] + temp_mask['WAFER_NUMBER']
                concat_col.append(len(temp_mask[fdc_concat_col].unique()))
            for col_no, col in enumerate(n_unique_cols):
                temp_all_rec =[]
                for each_rec in fdc_unique_cols_values:
                    temp_all_rec.append(each_rec[col_no])
                output[col] = temp_all_rec

            output[fdc_concat_col] = concat_col
            output['START_TIME'] = START_TIME
            output['END_TIME'] = END_TIME
            output['TIME_SEC'] = (output['END_TIME'] - output['START_TIME']).dt.total_seconds()
            output["TOOL_CHAMBER"]= module_id
            output["lam P/N"]= lam_pn
            output["Serial Number"]= serial_no
            output['nData']=nData
            output['Prior_nInstall'] = priorinstall
            output['Prior_Usage'] = final_prior_usage
            for m1 in range(len(mean_cols)):
                df1=df[df["SENSOR_NAME"]==mean_cols[m1]]
                df1 = df1.sort_values(by='START_TIME')
                df1.reset_index(drop=True,inplace=True)
                #create the diff and show in seconds 
                diff = df1['END_TIME'] - df1['START_TIME']
                TIME_SEC = (diff.dt.total_seconds())
                df1.insert(4,'TIME_SEC',TIME_SEC)
                value_max = []
                value_min = []
                value_mean=[]
                value_std = []
                value_slope=[]
                SUM_TIME_DIFF_SEC = []
                NO_OF_STEPS = []
                SUM_TIME_GAP_SEC = []
                prior_usage = []
                prior_nInstall = []
                append_prior_usage = 0
                for i in range(len(time_)-1):
                    mask_data = mask_date(start=time_[i],end=time_[i+1],dataframe=df1)

                    mask_data['TIME']=mask_data['START_TIME'].values.astype(int).tolist()
                    append_max = cal_max(data=mask_data,var="FDC_RAW",weight_var='TIME_SEC')
                    value_max.append(append_max)
                    ### min
                    append_min = cal_min(data=mask_data,var="FDC_RAW",weight_var='TIME_SEC')
                    value_min.append(append_min)
                    ### mean
                    append_mean = cal_wavg(data=mask_data,var="FDC_RAW",weight_var='TIME_SEC')
                    value_mean.append(append_mean)
                    ### Stdev
                    append_std = cal_weighted_std(data=mask_data,var="FDC_RAW",weight_var='TIME_SEC')
                    value_std.append(append_std)
                    ### Slope
                    append_slope = cal_slope(data=mask_data,var2="FDC_RAW",var1='TIME')
                    value_slope.append(append_slope)

                output[mean_cols[m1]+"_Min"] = value_min
                output[mean_cols[m1]+"_Max"] = value_max
                output[mean_cols[m1]+"_Mean"] = value_mean
                output[mean_cols[m1]+"_Stdev"] = value_std
                output[mean_cols[m1]+"_Slope"] = value_slope
                output[mean_cols[m1]+"_Slope"]=output[mean_cols[m1]+"_Max"]-output[mean_cols[m1]+"_Min"]

            if count_parts_created==1:
                out_put1=output
            else:
                out_put1=pd.concat([out_put1,output])        

            list_vars=list(set(out_put1.columns) - set(["START_TIME","END_TIME","TIME_SEC","TOOL_CHAMBER","lam P/N","Serial Number"]))
            x1=["nData","Prior_Usage","Prior_nInstall"] + [fdc_concat_col] + n_unique_cols
            list1 = [ele for ele in list_vars if ele not in x1] 
            list_allvars=["TOOL_CHAMBER","lam P/N","Serial Number","START_TIME","END_TIME","TIME_SEC"]+x1+list1
            out_put1=out_put1[list_allvars]
            Ndata_0 = (out_put1['nData'] == 0) 
            mod_data=out_put1[~Ndata_0]
            if mod_data.shape[0] > 0 :
                mod_data_created = True
    print("Total No of Days Data Available:", day_count)
    if mod_data_created:
        mod_data_grouped = mod_data.groupby(['TOOL_CHAMBER', 'lam P/N', 'Serial Number'])
        mod_data['TOTAL_nDATA'] = 0
        mod_data['TOTAL_nOUTLIER'] = 0
        mod_data['TOTAL_%OUTLIER'] = 0
        mod_data['NDD_Sum_POST_ETCH'] = 0
        mod_data['NDD_Surface_Particle_Sum_POST_ETCH'] = 0
        mod_data['NDD_Missing_Pattern_Sum_POST_ETCH'] = 0
        mod_data['NDD_Single_Line_Open_Sum_POST_ETCH'] = 0
        mod_data['NDD_Single_Line_Open_Type_2_Sum_POST_ETCH'] = 0
        fdc_df = master_fdc_df.copy()
        fdc_df['SCRIBE_STEP'] = fdc_df['SCRIBE'] + "_" + fdc_df['STEP']
        req_scribe_list = fdc_df['SCRIBE'].unique().tolist()
        
        space_df = get_inline_space_data(wafer_path, inline_space_path, tool_chamber, req_scribe_list, from_date, to_date)
        post_df = get_post_etch_data(post_etch_path, tool_chamber, req_scribe_list, from_date, to_date)
                
        for key, sub_df in mod_data_grouped:
            for ind, row in sub_df.iterrows():
                masked_df = fdc_df.loc[(fdc_df['START_TIME'] >= row['START_TIME']) & (fdc_df['END_TIME'] < row['END_TIME'])]
                fdc_ss = masked_df['SCRIBE_STEP'].unique().tolist()

                if space_df.shape[0] > 0:
                    inline_masked_df = space_df.loc[(space_df['DATE_STEP_FINISHED'] >= row['START_TIME']) & (space_df['DATE_STEP_FINISHED'] <= row['END_TIME'])]
                    inline_masked_df = inline_masked_df[inline_masked_df['SCRIBE_STEP'].isin(fdc_ss)]
                    mod_data['TOTAL_nDATA'].iloc[ind] = inline_masked_df['TOTAL_nDATA'].sum()
                    mod_data['TOTAL_nOUTLIER'].iloc[ind] = inline_masked_df['TOTAL_nOUTLIER'].sum()
                    mod_data['TOTAL_%OUTLIER'] = round(mod_data['TOTAL_nOUTLIER']/mod_data['TOTAL_nDATA'], 2)
                    mod_data['TOTAL_%OUTLIER'] = mod_data['TOTAL_%OUTLIER']*100

                if post_df.shape[0] > 0:
                    post_masked_df = post_df.loc[(post_df['INSPECTIONTIME'] >= row['START_TIME']) & (post_df['INSPECTIONTIME'] <= row['END_TIME'])]
                    post_masked_df = post_masked_df[post_masked_df['SCRIBE_STEP'].isin(fdc_ss)]
                    
                    NDD_Sum_value = post_masked_df.NDD.sum()
                    NDD_Surface_Particle_Sum_value = post_masked_df[post_masked_df['DEFECTDESCRIPTION'] == 'Surface Particle'].NDD.sum()
                    NDD_Missing_Pattern_Sum_value = post_masked_df[post_masked_df['DEFECTDESCRIPTION'] == 'Missing Pattern'].NDD.sum()
                    NDD_Single_Line_Open_Sum_value = post_masked_df[post_masked_df['DEFECTDESCRIPTION'] == 'Single Line Open'].NDD.sum()
                    NDD_Single_Line_Open_Type_2_Sum_value = post_masked_df[post_masked_df['DEFECTDESCRIPTION'] == 'Single Line Open Type 2'].NDD.sum()

                    mod_data['NDD_Sum_POST_ETCH'].iloc[ind] = NDD_Sum_value
                    mod_data['NDD_Surface_Particle_Sum_POST_ETCH'].iloc[ind] = NDD_Surface_Particle_Sum_value
                    mod_data['NDD_Missing_Pattern_Sum_POST_ETCH'].iloc[ind] = NDD_Missing_Pattern_Sum_value
                    mod_data['NDD_Single_Line_Open_Sum_POST_ETCH'].iloc[ind] = NDD_Single_Line_Open_Sum_value
                    mod_data['NDD_Single_Line_Open_Type_2_Sum_POST_ETCH'].iloc[ind] = NDD_Single_Line_Open_Type_2_Sum_value

        mod_data.rename(columns={'nData': 'nData_FDC',
                   'TOTAL_nDATA': 'TOTAL_nDATA_INLINE_SPACE',
                   'TOTAL_nOUTLIER': 'TOTAL_nOUTLIER_INLINE_SPACE',
                   'TOTAL_%OUTLIER': 'TOTAL_%OUTLIER_INLINE_SPACE'}, inplace=True)
    if mod_data_created:
        print("Etch Data Agg Success!")
        log.info("Succesfully aggregated data")
        agg_end_time = datetime.now()
        agg_time = agg_end_time- agg_start_time
        print("Time taken for Etch DataAgg : ", agg_time)
        
        try:
            pred_data=Pred_DataPreparation(mod_data)
        except Exception as e:
            log.error("Exception in get_aggr_data %s",e)
            print("Exception in get_aggr_data",e)
            #sys.exit()

        print("Etch Model Data Preparation Success!")
        pred_data=pd.DataFrame(pred_data)
        return pred_data
    else:
        print("NO FDC DATA FOR GIVEN TIME RANGE\nExiting!!!")
        log.error("Thre's no fdc data for the givem time range, exiting")
        raise Exception("NO FDC DATA FOR GIVEN TIME RANGE\nExiting!!!")
        #sys.exit()

    sys.exit()
    print("CHECKING DONE!!\nREMOVE EXIT STATEMENTS")


if __name__ == '__main__':
    df1 = get_aggr_data(ptms_file, wafer_path, inline_space_path, post_etch_path, tool_chamber, fdc_folder_path, prediction_data_end_date, input_usage,live_prediction)